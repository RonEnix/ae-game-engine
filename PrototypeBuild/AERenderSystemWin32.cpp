#include "AERenderSystemWin32.h"

#include <windows.h>
#include <GL/glu.h>

namespace AE {

AERenderSystem* AERenderSystem::mRenderSystem = NULL;

const AEString LOG_PREFIX("[AE RENDER SYSTEM] : ");

AERenderSystem::AERenderSystem()
: mContextList(10)
, mHandleInstance(0)
, mHandleWindow(0)
, mHandleDeviceContext(0)
, mCallBackProcedure(0)
, mScreenWidth(1)
, mScreenHeight(1)
, mFullScreen(false)
{
}

AERenderSystem::AERenderSystem(const AERenderSystem&)
: mContextList(10)
, mHandleInstance(0)
, mHandleWindow(0)
, mHandleDeviceContext(0)
, mCallBackProcedure(0)
, mScreenWidth(1)
, mScreenHeight(1)
, mFullScreen(false)
{
}

AERenderSystem& AERenderSystem::getInstance()
{
	if (mRenderSystem == NULL)
		mRenderSystem = new AERenderSystem();

	return *mRenderSystem;
}

void AERenderSystem::createWindow(const AEString& title, const AEUint screeWidth, const AEUint screenHeight, const bool fullscreen)
{
	AELOG(LOG_INFO) << LOG_PREFIX << "Creating Window.";

	//MessageBox(HWND_DESKTOP, "Creating Window.", "Log", MB_OK | MB_ICONEXCLAMATION);

	if (mHandleWindow != 0 || mHandleInstance == 0)
	{
		AELOG(LOG_ERROR) << LOG_PREFIX << "Handle to a windows context already exists or instance handle is invalid.";
		return;
	}

	const AEString className("AE OPENGL");

	mFullScreen = fullscreen;
	mScreenWidth = screeWidth;
	mScreenHeight = screenHeight;
	
	AELOG(LOG_INFO) << LOG_PREFIX << "Windows handle instance: " << mHandleInstance;

	// Register new window class from instance handle.
	WNDCLASSEX windowClass;
	ZeroMemory(&windowClass, sizeof(WNDCLASSEX));
	windowClass.cbSize			= sizeof (WNDCLASSEX);					// Size Of The windowClass Structure
	windowClass.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraws The Window For Any Movement / Resizing
	windowClass.lpfnWndProc		= mCallBackProcedure;					// WindowProc Handles Messages
	//windowClass.lpfnWndProc		= NULL;									// WindowProc Handles Messages. This is ignored temporarily.
	windowClass.hInstance		= mHandleInstance;						// Set The Instance
	windowClass.hbrBackground	= (HBRUSH)(COLOR_APPWORKSPACE);			// Class Background Brush Color
	windowClass.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	windowClass.lpszClassName	= className.c_str();					// Sets The Applications Classname

	if (RegisterClassEx(&windowClass) == 0)								// Did Registering The Class Fail?
	{
		// NOTE: Failure, Should Never Happen
		AELOG(LOG_ERROR) << LOG_PREFIX << "Failed to register new windows class.";
		return;
	}

	AELOG(LOG_INFO) << LOG_PREFIX << "Windows class registered.";

	DWORD windowStyle			(WS_OVERLAPPEDWINDOW);
	DWORD windowExtendedStyle	(WS_EX_APPWINDOW);
	const AEInt bitsPerPixel(16);
	// Set windows dimensions:
	RECT windowRect = {0, 0, mScreenWidth, screenHeight};

	if (mFullScreen == true)
	{
		DEVMODE deviceScreenMode;
		ZeroMemory(&deviceScreenMode, sizeof(DEVMODE));
		deviceScreenMode.dmSize			= sizeof(DEVMODE);	// Size Of The Devmode Structure
		deviceScreenMode.dmPelsWidth	= mScreenWidth;			// Select Screen Width
		deviceScreenMode.dmPelsHeight	= mScreenHeight;			// Select Screen Height
		deviceScreenMode.dmBitsPerPel	= bitsPerPixel;		// Select Bits Per Pixel
		deviceScreenMode.dmFields		= DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		// Apply settings:
		if (ChangeDisplaySettings(&deviceScreenMode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
			AELOG(LOG_ERROR) << LOG_PREFIX << "Unabled to apply fullscreen mode. Using windowed mode instead.";
			mFullScreen = false;
		}
		else
		{	// Fullscreen enabled.
			ShowCursor(FALSE);					 	// Turn Off The Cursor
			windowStyle = WS_POPUP;				 	// Set The WindowStyle To WS_POPUP (Popup Window)
			windowExtendedStyle |= WS_EX_TOPMOST;	// Set The Extended Window Style To WS_EX_TOPMOST
		}
	}
	else
		AdjustWindowRectEx(&windowRect, windowStyle, 0, windowExtendedStyle);

	// Create The OpenGL Window
	AELOG(LOG_INFO) << LOG_PREFIX << "Creating OpenGL window.";
	mHandleWindow = CreateWindowEx(windowExtendedStyle,					// Extended Style
								   className.c_str(),					// Class Name
								   title.c_str(),						// Window Title
								   windowStyle,							// Window Style
								   0, 0,								// Window X,Y Position
								   windowRect.right - windowRect.left,	// Window Width
								   windowRect.bottom - windowRect.top,	// Window Height
								   HWND_DESKTOP,						// Desktop Is Window's Parent
								   0,									// No Menu
								   mHandleInstance,						// Pass The Window Instance
								   NULL);
	if (mHandleWindow == 0)
	{
		AELOG(LOG_ERROR) << LOG_PREFIX << "Failed to create window screen.";
		mHandleWindow = 0;
		return;
	}
	// Grab device context:
	mHandleDeviceContext = GetDC(mHandleWindow);		// Grab A Device Context For This Window
	if (mHandleDeviceContext == 0)						// Did We Get A Device Context?
	{
		// Failed
		DestroyWindow(mHandleWindow);					// Destroy The Window
		mHandleWindow = 0;								// Zero The Window Handle
		AELOG(LOG_ERROR) << LOG_PREFIX << "Failed to retrieve device context.";
		return;
	}

	const PIXELFORMATDESCRIPTOR pixelFormatDescriptor =
	{
		sizeof (PIXELFORMATDESCRIPTOR),		// Size Of This Pixel Format Descriptor
		1,									// Version Number
		PFD_DRAW_TO_WINDOW |				// Format Must Support Window
		PFD_SUPPORT_OPENGL |				// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,					// Must Support Double Buffering
		PFD_TYPE_RGBA,						// Request An RGBA Format
		16,									// Select Our Color Depth
		0, 0, 0, 0, 0, 0,					// Color Bits Ignored
		0,									// No Alpha Buffer
		0,									// Shift Bit Ignored
		0,									// No Accumulation Buffer
		0, 0, 0, 0,							// Accumulation Bits Ignored
		bitsPerPixel,						// 16Bit Z-Buffer (Depth Buffer)  
		0,									// No Stencil Buffer
		0,									// No Auxiliary Buffer
		PFD_MAIN_PLANE,						// Main Drawing Layer
		0,									// Reserved
		0, 0, 0								// Layer Masks Ignored
	};

	const AEInt pixelFormat(ChoosePixelFormat(mHandleDeviceContext, &pixelFormatDescriptor));		// Find A Compatible Pixel Format
	if (pixelFormat == 0)												// Did We Find A Compatible Format?
	{
		// Failed
		AELOG(LOG_ERROR) << LOG_PREFIX << "Failed to setup pixel format information.";
		ReleaseDC(mHandleWindow, mHandleDeviceContext);					// Release Our Device Context
		mHandleDeviceContext = 0;										// Zero The Device Context
		DestroyWindow(mHandleWindow);									// Destroy The Window
		mHandleWindow = 0;												// Zero The Window Handle
		return;
	}

	if (SetPixelFormat(mHandleDeviceContext, pixelFormat, &pixelFormatDescriptor) == FALSE)		// Try To Set The Pixel Format
	{
		// Failed
		AELOG(LOG_ERROR) << LOG_PREFIX << "Failed to setup pixel format information.";
		ReleaseDC(mHandleWindow, mHandleDeviceContext);					// Release Our Device Context
		mHandleDeviceContext = 0;										// Zero The Device Context
		DestroyWindow(mHandleWindow);									// Destroy The Window
		mHandleWindow = 0;												// Zero The Window Handle
		return;													// Return False
	}
}

void AERenderSystem::destroyWindow()
{
	if (mHandleWindow == 0 || mHandleDeviceContext == 0)
	{
		AELOG(LOG_WARNING) << LOG_PREFIX << "Windows handles already destroyed.";
		return;
	}

	// Clear all created render contexts.
	if (mContextList.size() > 0)
	{
		wglMakeCurrent(mHandleDeviceContext, 0);						// Set The Current Active Rendering Context To Zero
		for (std::vector<HGLRC>::iterator itrContext(mContextList.begin()); itrContext != mContextList.end(); ++itrContext)
		{
			wglDeleteContext(*itrContext);								// Release The Rendering Context
			*itrContext = 0;
			AELOG(LOG_INFO) << LOG_PREFIX << "Context at mem address " << std::hex << *itrContext << " destroyed.";
		}

		mContextList.clear();

	}

	ReleaseDC(mHandleWindow, mHandleDeviceContext);					// Release Our Device Context
	mHandleDeviceContext = 0;										// Zero The Device Context
	DestroyWindow(mHandleWindow);									// Destroy The Window
	mHandleWindow = 0;												// Zero The Window Handle

	if (mFullScreen)
	{
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(TRUE);
	}

	const AEString className("AE OPENGL");
	UnregisterClass(className.c_str(), mHandleInstance);		// UnRegister Window Class

	AELOG(LOG_INFO) << LOG_PREFIX << "Display and window clean destroy.";
}

AEContextHandle AERenderSystem::createRenderContext()
{
	AEContextHandle returnHandle(INVALID);

	if (mHandleWindow == 0 || mHandleDeviceContext == 0)
	{
		AELOG(LOG_WARNING) << LOG_PREFIX << "Unabled to create new render context. Windows handles and Device context unintialised.";
	}
	else
	{
		const HGLRC newRenderContext(wglCreateContext(mHandleDeviceContext));	// Try To Get A Rendering Context
		if (newRenderContext == 0)													// Did We Get A Rendering Context?
		{
			// Failed
			ReleaseDC(mHandleWindow, mHandleDeviceContext);	// Release Our Device Context
			mHandleDeviceContext = 0;						// Zero The Device Context
			DestroyWindow(mHandleWindow);					// Destroy The Window
			mHandleWindow = 0;								// Zero The Window Handle
			AELOG(LOG_ERROR) << LOG_PREFIX << "Failed to create new render context.";
		}
		else
		{
			mContextList.push_back(newRenderContext);
			returnHandle = static_cast<AEContextHandle>(mContextList.size());
		}
	}

	return returnHandle;
}

void AERenderSystem::setCurrentContext(const AEContextHandle contextHandle)
{
	if (mHandleWindow == 0 || mHandleDeviceContext == 0) {
		AELOG(LOG_WARNING) << LOG_PREFIX << "Unabled to set render context. Windows handles and Device context unintialised.";
		AELOG(LOG_WARNING) << LOG_PREFIX << "Window Handle: " << mHandleWindow << " Device Context: " << mHandleDeviceContext;
	}
	else
	{
		// Make the context as the current render context.
		HGLRC& currentContext(mContextList.at(contextHandle - 1));

		if (wglMakeCurrent(mHandleDeviceContext, currentContext) == FALSE)
		{
			wglDeleteContext(currentContext);								// Delete The Rendering Context
			currentContext = 0;												// Zero The Rendering Context
			ReleaseDC(mHandleWindow, mHandleDeviceContext);					// Release Our Device Context
			mHandleDeviceContext = 0;										// Zero The Device Context
			DestroyWindow(mHandleWindow);									// Destroy The Window
			mHandleWindow = 0;												// Zero The Window Handle
			AELOG(LOG_ERROR) << LOG_PREFIX << "Error trying to set active render context.";
		}
		else
		{
			// Show it now.
			ShowWindow(mHandleWindow, SW_NORMAL);
		}
#if 0
		{
			GLenum error(glewInit());
			if (GLEW_OK != error)
			{
				AELOG(LOG_INFO) << LOG_PREFIX << "Failed to initialise GLEW. " << glewGetErrorString(error) << ".";
			}

			AELOG(LOG_INFO) << LOG_PREFIX << "GLEW Version: " << glewGetString(GLEW_VERSION) << ".";
		}
#endif
	}
}

void AERenderSystem::initialiseRenderer()
{
	// Default OpenGL enabled modes
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glFrontFace(GL_CCW);							// Force counter-clockwise winding fronts.
	glClearDepth(1.0F);

	//glEnable(GL_TEXTURE_2D);

	//glEnable(GL_COLOR_MATERIAL);
	//glColorMaterial(GL_FRONT_AND_BACK, GL_EMISSION);
	
	{	// Temp stuff
		// Multisampling
		//glEnable(GL_MULTISAMPLE_ARB);
	}

	// Perspective calculation setting:
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resizeRenderSystem(mScreenWidth, mScreenHeight);
	glFlush();
}

void AERenderSystem::setClearColor(const AEColor& color)
{
	glClearColor(color.red(), color.blue(), color.green(), color.alpha());
}

void AERenderSystem::enableRenderParameter(const AEUint params)
{
	if (RENDER_PARAM_DEPTH_TEST & params)
		enableDepthTest();
	if (RENDER_PARAM_BACK_FACE_CULLING & params)
		enableBackFaceCulling();
	if (RENDER_PARAM_ALPHA_BLEND & params)
		enableAlphaBlend();
	if (RENDER_PARAM_LINE_SMOOTHING & params)
		enableLineSmoothing();
	if (RENDER_PARAM_WIRE_FRAME & params)
		enableWireFrameMode();
	if (RENDER_PARAM_LIGHTING & params)
		enableLighting();
	if (RENDER_PARAM_TEXTURING & params)
		enableTexture();
}

void AERenderSystem::disableRenderParameter(const AEUint params)
{
	if (RENDER_PARAM_DEPTH_TEST & params)
		disableDepthTest();
	if (RENDER_PARAM_BACK_FACE_CULLING & params)
		disableBackFaceCulling();
	if (RENDER_PARAM_ALPHA_BLEND & params)
		disableAlphaBlend();
	if (RENDER_PARAM_LINE_SMOOTHING & params)
		disableLineSmoothing();
	if (RENDER_PARAM_WIRE_FRAME & params)
		disableWireFrameMode();
	if (RENDER_PARAM_LIGHTING & params)
		disableLighting();
	if (RENDER_PARAM_TEXTURING & params)
		disableTexture();
}

void AERenderSystem::enableLight(const AEInt light)
{
	switch (light)
	{
		case 0:
			glEnable(GL_LIGHT0);
			break;
		case 1:
			glEnable(GL_LIGHT1);
			break;
		case 2:
			glEnable(GL_LIGHT2);
			break;
		case 3:
			glEnable(GL_LIGHT3);
			break;
		case 4:
			glEnable(GL_LIGHT4);
			break;
		case 5:
			glEnable(GL_LIGHT5);
			break;
		case 6:
			glEnable(GL_LIGHT6);
			break;
		case 7:
			glEnable(GL_LIGHT7);
			break;
		default:
			break;
	}
}

void AERenderSystem::disableLight(const AEInt light)
{
	switch (light)
	{
		case 0:
			glDisable(GL_LIGHT0);
			break;
		case 1:
			glDisable(GL_LIGHT1);
			break;
		case 2:
			glDisable(GL_LIGHT2);
			break;
		case 3:
			glDisable(GL_LIGHT3);
			break;
		case 4:
			glDisable(GL_LIGHT4);
			break;
		case 5:
			glDisable(GL_LIGHT5);
			break;
		case 6:
			glDisable(GL_LIGHT6);
			break;
		case 7:
			glDisable(GL_LIGHT7);
			break;
		default:
			break;
	}
}

void AERenderSystem::startFrame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
}

void AERenderSystem::endFrame()
{
	glFinish();
	SwapBuffers(mHandleDeviceContext);
}

void AERenderSystem::resizeRenderSystem(AEInt width, AEInt height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0F, static_cast<GLfloat>(width)/static_cast<GLfloat>(height), 0.01F, 100.0F);
	glMatrixMode(GL_MODELVIEW);

}

void AERenderSystem::enableDepthTest()
{
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
}

void AERenderSystem::enableBackFaceCulling()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void AERenderSystem::enableAlphaBlend()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void AERenderSystem::enableLineSmoothing()
{
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glLineWidth(1.0F);
}

void AERenderSystem::enableWireFrameMode()
{
	glPolygonMode(GL_FRONT, GL_LINE);
}

void AERenderSystem::enableLighting()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, AEColor(0.1F, 0.1F, 0.4F, 1.0F));
}

void AERenderSystem::enableTexture()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void AERenderSystem::disableDepthTest()
{
	glDisable(GL_DEPTH_TEST);
	glClearDepth(1.0F);
}

void AERenderSystem::disableBackFaceCulling()
{
	glDisable(GL_CULL_FACE);
}

void AERenderSystem::disableAlphaBlend()
{
	glDisable(GL_BLEND);
}

void AERenderSystem::disableLineSmoothing()
{
	glDisable(GL_LINE_SMOOTH);
	glLineWidth(1.0F);
}

void AERenderSystem::disableWireFrameMode()
{
	glPolygonMode(GL_FRONT, GL_FILL);
}

void AERenderSystem::disableLighting()
{
	glDisable(GL_LIGHTING);
}

void AERenderSystem::disableTexture()
{
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

} // namespace AE

