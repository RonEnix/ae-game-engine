#ifndef AE_LIGHT_H_
#define AE_LIGHT_H_

#include "AESystemTypes.h"

namespace AE
{

class AELight
{
	public:
		enum LightType
		{
			  POINT_LIGHT = 0
			, DIRECTIONAL_LIGHT
			, SPOT_LIGHT
		};

		AELight()
		: mAmbientColor(COLOR_BLACK)
		, mDiffuseColor(COLOR_BLACK)
		, mSpecularColor(COLOR_BLACK)
		, mPosition(AEVector3::ZERO)
		, mDirection(AEVector3::UNIT_X)
		, mAttenuation(0.0F)
		, mType(POINT_LIGHT)
		{}

		AELight(const LightType lightType, const AEColor& ambientColor=COLOR_BLACK, const AEColor& diffuseColor=COLOR_BLACK,
				const AEColor& specularColor=COLOR_WHITE, const AEVector3& position=AEVector3::ZERO,
				const AEVector3& direction=AEVector3::UNIT_Z, const AEFloat32 attenuation=0.0F)
		: mAmbientColor(ambientColor)
		, mDiffuseColor(diffuseColor)
		, mSpecularColor(specularColor)
		, mPosition(position)
		, mDirection(direction)
		, mAttenuation(attenuation)
		, mType(lightType)
		{}

		const AEColor& getAmbientColor()	const { return mAmbientColor; }
		const AEColor& getDiffuseColor()	const { return mDiffuseColor; }
		const AEColor& getSpecularColor()	const { return mSpecularColor; }

		AEColor& getAmbientColor()			{ return mAmbientColor; }
		AEColor& getDiffuseColor()			{ return mDiffuseColor; }
		AEColor& getSpecularColor()			{ return mSpecularColor; }

		const AEVector3& getPosition()		const { return mPosition; }
		const AEVector3& getDirection()		const { return mDirection; }

		AEFloat32 getAttenuation()			const { return mAttenuation; }
		LightType getLightType()			const { return mType; }

	private:
		AEColor		mAmbientColor;
		AEColor		mDiffuseColor;
		AEColor		mSpecularColor;
		AEVector3	mPosition;
		AEVector3	mDirection;
		AEFloat32	mAttenuation;
		LightType	mType;

};

} // namespace AE

#endif // AE_LIGHT_H_

