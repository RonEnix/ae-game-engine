#ifndef AE_BITMAP_FONT_LINUX_H_
#define AE_BITMAP_FONT_LINUX_H_

#include "AESystemTypes.h"

namespace AE
{

// Font used for debugging purposes. Not intended for graphical display in huds.
class AEBitMapFont
{
	public:
		AEBitMapFont()
		: mInitialised(false)
		, mDisplayIndex(0U)
		{}

		~AEBitMapFont();

		void initialise();
		void fontPrint(const AEString& text);

	private:
		bool mInitialised;
		AEUint mDisplayIndex;
};

} // namespace AE

#endif // AE_BITMAP_FONT_LINUX_H_

