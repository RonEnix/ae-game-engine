#include "AEMatrix4x4.h"

#include <memory.h>

namespace AE
{

AEMatrix4x4::AEMatrix4x4(const AEFloat32* contents)
: mFloatArray()
{
	mFloatArray = AEFixedArray<AEFloat32>(16U);
	memcpy(mFloatArray.begin(), contents, 16U * sizeof(AEFloat32));
}

void AEMatrix4x4::setColumn(const AEUint column, const AEVector4& content)
{
	switch (column)
	{
		case 0U:
			mFloatArray.at(0U) = content.x();
			mFloatArray.at(1U) = content.y();
			mFloatArray.at(2U) = content.z();
			mFloatArray.at(3U) = content.w();
			break;
		case 1U:
			mFloatArray.at(4U) = content.x();
			mFloatArray.at(5U) = content.y();
			mFloatArray.at(6U) = content.z();
			mFloatArray.at(7U) = content.w();
			break;
		case 2U:
			mFloatArray.at(8U) = content.x();
			mFloatArray.at(9U) = content.y();
			mFloatArray.at(10U) = content.z();
			mFloatArray.at(11U) = content.w();
			break;
		case 3U:
			mFloatArray.at(12U) = content.x();
			mFloatArray.at(13U) = content.y();
			mFloatArray.at(14U) = content.z();
			mFloatArray.at(15U) = content.w();
			break;
		default:
			break;
	}
}

void AEMatrix4x4::setRow(const AEUint row, const AEVector4& content)
{
	switch (row)
	{
		case 0U:
			mFloatArray.at(0U) = content.x();
			mFloatArray.at(4U) = content.y();
			mFloatArray.at(8U) = content.z();
			mFloatArray.at(12U) = content.w();
			break;
		case 1U:
			mFloatArray.at(1U) = content.x();
			mFloatArray.at(5U) = content.y();
			mFloatArray.at(9U) = content.z();
			mFloatArray.at(13U) = content.w();
			break;
		case 2U:
			mFloatArray.at(2U) = content.x();
			mFloatArray.at(6U) = content.y();
			mFloatArray.at(10U) = content.z();
			mFloatArray.at(14U) = content.w();
			break;
		case 3U:
			mFloatArray.at(3U) = content.x();
			mFloatArray.at(7U) = content.y();
			mFloatArray.at(11U) = content.z();
			mFloatArray.at(15U) = content.w();
			break;
		default:
			break;
	}
}

void AEMatrix4x4::setToIdentity()
{
	setToZero();
	mFloatArray.at(0U) = 1.0F;
	mFloatArray.at(5U) = 1.0F;
	mFloatArray.at(10U) = 1.0F;
	mFloatArray.at(15U) = 1.0F;
}

void AEMatrix4x4::setToZero()
{
	// Set all values to 0.0F
	for (AEUint index=0; index < 16; ++index)
		mFloatArray.pushBack(0.0F);
}

AEVector4 AEMatrix4x4::getColumn(const AEUint column) const
{
	switch (column)
	{
		case 0U:
			return AEVector4(mFloatArray.at(0U), mFloatArray.at(1U), mFloatArray.at(2U), mFloatArray.at(3U));
			break;
		case 1U:
			return AEVector4(mFloatArray.at(4U), mFloatArray.at(5U), mFloatArray.at(6U), mFloatArray.at(7U));
			break;
		case 2U:
			return AEVector4(mFloatArray.at(8U), mFloatArray.at(9U), mFloatArray.at(10U), mFloatArray.at(11U));
			break;
		case 3U:
			return AEVector4(mFloatArray.at(12U), mFloatArray.at(13U), mFloatArray.at(14U), mFloatArray.at(15U));
			break;
		default:
			return AEVector4();
			break;
	}
}

AEVector4 AEMatrix4x4::getRow(const AEUint row) const
{
	switch (row)
	{
		case 0U:
			return AEVector4(mFloatArray.at(0U), mFloatArray.at(4U), mFloatArray.at(8U), mFloatArray.at(12U));
			break;
		case 1U:
			return AEVector4(mFloatArray.at(1U), mFloatArray.at(5U), mFloatArray.at(9U), mFloatArray.at(13U));
			break;
		case 2U:
			return AEVector4(mFloatArray.at(2U), mFloatArray.at(6U), mFloatArray.at(10U), mFloatArray.at(14U));
			break;
		case 3U:
			return AEVector4(mFloatArray.at(3U), mFloatArray.at(7U), mFloatArray.at(11U), mFloatArray.at(15U));
			break;
		default:
			return AEVector4();
			break;
	}
}

AEMatrix4x4 operator*(const AEFloat32 scale, const AEMatrix4x4& rMatrix)
{
	return rMatrix * scale;
}

AEMatrix4x4 AEMatrix4x4::operator+(const AEMatrix4x4& rMatrix) const
{
	AEMatrix4x4 returnMatrix;
	returnMatrix.setValue(0U, mFloatArray.at(0U) + rMatrix.getValue(0U));
	returnMatrix.setValue(1U, mFloatArray.at(1U) + rMatrix.getValue(1U));
	returnMatrix.setValue(2U, mFloatArray.at(2U) + rMatrix.getValue(2U));
	returnMatrix.setValue(3U, mFloatArray.at(3U) + rMatrix.getValue(3U));
	returnMatrix.setValue(4U, mFloatArray.at(4U) + rMatrix.getValue(4U));
	returnMatrix.setValue(5U, mFloatArray.at(5U) + rMatrix.getValue(5U));
	returnMatrix.setValue(6U, mFloatArray.at(6U) + rMatrix.getValue(6U));
	returnMatrix.setValue(7U, mFloatArray.at(7U) + rMatrix.getValue(7U));
	returnMatrix.setValue(8U, mFloatArray.at(8U) + rMatrix.getValue(8U));
	returnMatrix.setValue(9U, mFloatArray.at(9U) + rMatrix.getValue(9U));
	returnMatrix.setValue(10U, mFloatArray.at(10U) + rMatrix.getValue(10U));
	returnMatrix.setValue(11U, mFloatArray.at(11U) + rMatrix.getValue(11U));
	returnMatrix.setValue(12U, mFloatArray.at(12U) + rMatrix.getValue(12U));
	returnMatrix.setValue(13U, mFloatArray.at(13U) + rMatrix.getValue(13U));
	returnMatrix.setValue(14U, mFloatArray.at(14U) + rMatrix.getValue(14U));
	returnMatrix.setValue(15U, mFloatArray.at(15U) + rMatrix.getValue(15U));
	return returnMatrix;
}

AEMatrix4x4 AEMatrix4x4::operator-(const AEMatrix4x4& rMatrix) const
{
	AEMatrix4x4 returnMatrix;
	returnMatrix.setValue(0U, mFloatArray.at(0U) - rMatrix.getValue(0U));
	returnMatrix.setValue(1U, mFloatArray.at(1U) - rMatrix.getValue(1U));
	returnMatrix.setValue(2U, mFloatArray.at(2U) - rMatrix.getValue(2U));
	returnMatrix.setValue(3U, mFloatArray.at(3U) - rMatrix.getValue(3U));
	returnMatrix.setValue(4U, mFloatArray.at(4U) - rMatrix.getValue(4U));
	returnMatrix.setValue(5U, mFloatArray.at(5U) - rMatrix.getValue(5U));
	returnMatrix.setValue(6U, mFloatArray.at(6U) - rMatrix.getValue(6U));
	returnMatrix.setValue(7U, mFloatArray.at(7U) - rMatrix.getValue(7U));
	returnMatrix.setValue(8U, mFloatArray.at(8U) - rMatrix.getValue(8U));
	returnMatrix.setValue(9U, mFloatArray.at(9U) - rMatrix.getValue(9U));
	returnMatrix.setValue(10U, mFloatArray.at(10U) - rMatrix.getValue(10U));
	returnMatrix.setValue(11U, mFloatArray.at(11U) - rMatrix.getValue(11U));
	returnMatrix.setValue(12U, mFloatArray.at(12U) - rMatrix.getValue(12U));
	returnMatrix.setValue(13U, mFloatArray.at(13U) - rMatrix.getValue(13U));
	returnMatrix.setValue(14U, mFloatArray.at(14U) - rMatrix.getValue(14U));
	returnMatrix.setValue(15U, mFloatArray.at(15U) - rMatrix.getValue(15U));
	return returnMatrix;
}

AEMatrix4x4 AEMatrix4x4::operator*(const AEMatrix4x4& rMatrix) const
{
	AEMatrix4x4 returnMatrix;
	returnMatrix.setValue(0U, (mFloatArray.at(0U) * rMatrix.getValue(0U)) + (mFloatArray.at(4U) * rMatrix.getValue(1U)) + (mFloatArray.at(8U) * rMatrix.getValue(2U)) + (mFloatArray.at(12U) * rMatrix.getValue(3U))); 
	returnMatrix.setValue(1U, (mFloatArray.at(1U) * rMatrix.getValue(0U)) + (mFloatArray.at(5U) * rMatrix.getValue(1U)) + (mFloatArray.at(9U) * rMatrix.getValue(2U)) + (mFloatArray.at(13U) * rMatrix.getValue(3U))); 
	returnMatrix.setValue(2U, (mFloatArray.at(2U) * rMatrix.getValue(0U)) + (mFloatArray.at(6U) * rMatrix.getValue(1U)) + (mFloatArray.at(10U) * rMatrix.getValue(2U)) + (mFloatArray.at(14U) * rMatrix.getValue(3U))); 
	returnMatrix.setValue(3U, (mFloatArray.at(3U) * rMatrix.getValue(0U)) + (mFloatArray.at(7U) * rMatrix.getValue(1U)) + (mFloatArray.at(11U) * rMatrix.getValue(2U)) + (mFloatArray.at(15U) * rMatrix.getValue(3U))); 
	returnMatrix.setValue(4U, (mFloatArray.at(0U) * rMatrix.getValue(4U)) + (mFloatArray.at(4U) * rMatrix.getValue(5U)) + (mFloatArray.at(8U) * rMatrix.getValue(6U)) + (mFloatArray.at(12U) * rMatrix.getValue(7U))); 
	returnMatrix.setValue(5U, (mFloatArray.at(1U) * rMatrix.getValue(4U)) + (mFloatArray.at(5U) * rMatrix.getValue(5U)) + (mFloatArray.at(9U) * rMatrix.getValue(6U)) + (mFloatArray.at(13U) * rMatrix.getValue(7U))); 
	returnMatrix.setValue(6U, (mFloatArray.at(2U) * rMatrix.getValue(4U)) + (mFloatArray.at(6U) * rMatrix.getValue(5U)) + (mFloatArray.at(10U) * rMatrix.getValue(6U)) + (mFloatArray.at(14U) * rMatrix.getValue(7U))); 
	returnMatrix.setValue(7U, (mFloatArray.at(3U) * rMatrix.getValue(4U)) + (mFloatArray.at(7U) * rMatrix.getValue(5U)) + (mFloatArray.at(11U) * rMatrix.getValue(6U)) + (mFloatArray.at(15U) * rMatrix.getValue(7U))); 
	returnMatrix.setValue(8U, (mFloatArray.at(0U) * rMatrix.getValue(8U)) + (mFloatArray.at(4U) * rMatrix.getValue(9U)) + (mFloatArray.at(8U) * rMatrix.getValue(10U)) + (mFloatArray.at(12U) * rMatrix.getValue(11U))); 
	returnMatrix.setValue(9U, (mFloatArray.at(1U) * rMatrix.getValue(8U)) + (mFloatArray.at(5U) * rMatrix.getValue(9U)) + (mFloatArray.at(9U) * rMatrix.getValue(10U)) + (mFloatArray.at(13U) * rMatrix.getValue(11U))); 
	returnMatrix.setValue(10U, (mFloatArray.at(2U) * rMatrix.getValue(8U)) + (mFloatArray.at(6U) * rMatrix.getValue(9U)) + (mFloatArray.at(10U) * rMatrix.getValue(10U)) + (mFloatArray.at(14U) * rMatrix.getValue(11U))); 
	returnMatrix.setValue(11U, (mFloatArray.at(3U) * rMatrix.getValue(8U)) + (mFloatArray.at(7U) * rMatrix.getValue(9U)) + (mFloatArray.at(11U) * rMatrix.getValue(10U)) + (mFloatArray.at(15U) * rMatrix.getValue(11U))); 
	returnMatrix.setValue(12U, (mFloatArray.at(0U) * rMatrix.getValue(12U)) + (mFloatArray.at(4U) * rMatrix.getValue(13U)) + (mFloatArray.at(8U) * rMatrix.getValue(14U)) + (mFloatArray.at(12U) * rMatrix.getValue(15U))); 
	returnMatrix.setValue(13U, (mFloatArray.at(1U) * rMatrix.getValue(12U)) + (mFloatArray.at(5U) * rMatrix.getValue(13U)) + (mFloatArray.at(9U) * rMatrix.getValue(14U)) + (mFloatArray.at(13U) * rMatrix.getValue(15U))); 
	returnMatrix.setValue(14U, (mFloatArray.at(2U) * rMatrix.getValue(12U)) + (mFloatArray.at(6U) * rMatrix.getValue(13U)) + (mFloatArray.at(10U) * rMatrix.getValue(14U)) + (mFloatArray.at(14U) * rMatrix.getValue(15U))); 
	returnMatrix.setValue(15U, (mFloatArray.at(3U) * rMatrix.getValue(12U)) + (mFloatArray.at(7U) * rMatrix.getValue(13U)) + (mFloatArray.at(11U) * rMatrix.getValue(14U)) + (mFloatArray.at(15U) * rMatrix.getValue(15U))); 
	return returnMatrix;
}

AEMatrix4x4 AEMatrix4x4::operator*(const AEFloat32 rValue) const
{
	AEMatrix4x4 returnMatrix;
	returnMatrix.setValue(0U, mFloatArray.at(0U) * rValue);
	returnMatrix.setValue(1U, mFloatArray.at(1U) * rValue);
	returnMatrix.setValue(2U, mFloatArray.at(2U) * rValue);
	returnMatrix.setValue(3U, mFloatArray.at(3U) * rValue);
	returnMatrix.setValue(4U, mFloatArray.at(4U) * rValue);
	returnMatrix.setValue(5U, mFloatArray.at(5U) * rValue);
	returnMatrix.setValue(6U, mFloatArray.at(6U) * rValue);
	returnMatrix.setValue(7U, mFloatArray.at(7U) * rValue);
	returnMatrix.setValue(8U, mFloatArray.at(8U) * rValue);
	returnMatrix.setValue(9U, mFloatArray.at(9U) * rValue);
	returnMatrix.setValue(10U, mFloatArray.at(10U) * rValue);
	returnMatrix.setValue(11U, mFloatArray.at(11U) * rValue);
	returnMatrix.setValue(12U, mFloatArray.at(12U) * rValue);
	returnMatrix.setValue(13U, mFloatArray.at(13U) * rValue);
	returnMatrix.setValue(14U, mFloatArray.at(14U) * rValue);
	returnMatrix.setValue(15U, mFloatArray.at(15U) * rValue);
	return returnMatrix;
}

AEMatrix4x4 AEMatrix4x4::operator/(const AEFloat32 rValue) const
{
	if ((rValue == 0.0F) && (rValue == 1.0F))
		return (*this);

	const AEFloat32 invValue(1.0F / rValue);
	return (*this) * invValue;
}

AEMatrix4x4 AEMatrix4x4::operator-() const
{
	AEMatrix4x4 returnMatrix;
	returnMatrix.setValue(0U, -mFloatArray.at(0U));
	returnMatrix.setValue(1U, -mFloatArray.at(1U));
	returnMatrix.setValue(2U, -mFloatArray.at(2U));
	returnMatrix.setValue(3U, -mFloatArray.at(3U));
	returnMatrix.setValue(4U, -mFloatArray.at(4U));
	returnMatrix.setValue(5U, -mFloatArray.at(5U));
	returnMatrix.setValue(6U, -mFloatArray.at(6U));
	returnMatrix.setValue(7U, -mFloatArray.at(7U));
	returnMatrix.setValue(8U, -mFloatArray.at(8U));
	returnMatrix.setValue(9U, -mFloatArray.at(9U));
	returnMatrix.setValue(10U, -mFloatArray.at(10U));
	returnMatrix.setValue(11U, -mFloatArray.at(11U));
	returnMatrix.setValue(12U, -mFloatArray.at(12U));
	returnMatrix.setValue(13U, -mFloatArray.at(13U));
	returnMatrix.setValue(14U, -mFloatArray.at(14U));
	returnMatrix.setValue(15U, -mFloatArray.at(15U));
	return returnMatrix;
}

AEVector4 AEMatrix4x4::operator*(const AEVector4& rVector) const
{
	return AEVector4(mFloatArray.at(0U) * rVector.x() +		// X
					 mFloatArray.at(4U) * rVector.y() +
					 mFloatArray.at(8U) * rVector.z() +
					 mFloatArray.at(12U) * rVector.w(),
					 mFloatArray.at(1U) * rVector.x() +		// Y
					 mFloatArray.at(5U) * rVector.y() +
					 mFloatArray.at(9U) * rVector.z() +
					 mFloatArray.at(13U) * rVector.w(),
					 mFloatArray.at(2U) * rVector.x() +		// Z
					 mFloatArray.at(6U) * rVector.y() +
					 mFloatArray.at(10U) * rVector.z() +
					 mFloatArray.at(14U) * rVector.w(),
					 mFloatArray.at(3U) * rVector.x() +		// W
					 mFloatArray.at(7U) * rVector.y() +
					 mFloatArray.at(11U) * rVector.z() +
					 mFloatArray.at(15U) * rVector.w());

}

AEVector3 AEMatrix4x4::operator*(const AEVector3 rVector) const
{
	return AEVector3(mFloatArray.at(0U) * rVector.x() + mFloatArray.at(4U) * rVector.y() + mFloatArray.at(8U) * rVector.z(),
					 mFloatArray.at(1U) * rVector.x() + mFloatArray.at(5U) * rVector.y() + mFloatArray.at(9U) * rVector.z(),
					 mFloatArray.at(2U) * rVector.x() + mFloatArray.at(6U) * rVector.y() + mFloatArray.at(10U) * rVector.z());
}


AEVector3 AEMatrix4x4::translateVector3(const AEVector3& vector) const
{
	return AEVector3(mFloatArray.at(12U) + vector.x(), mFloatArray.at(13U) + vector.y(), mFloatArray.at(14U) + vector.z());
}

AEMatrix4x4 AEMatrix4x4::transpose() const
{
	AEMatrix4x4 newMatrix;
	newMatrix.setValue(0U, mFloatArray.at(0U));
	newMatrix.setValue(1U, mFloatArray.at(4U));
	newMatrix.setValue(2U, mFloatArray.at(8U));
	newMatrix.setValue(3U, mFloatArray.at(12U));
	newMatrix.setValue(4U, mFloatArray.at(1U));
	newMatrix.setValue(5U, mFloatArray.at(5U));
	newMatrix.setValue(6U, mFloatArray.at(9U));
	newMatrix.setValue(7U, mFloatArray.at(13U));
	newMatrix.setValue(8U, mFloatArray.at(2U));
	newMatrix.setValue(9U, mFloatArray.at(5U));
	newMatrix.setValue(10U, mFloatArray.at(9U));
	newMatrix.setValue(11U, mFloatArray.at(13U));
	newMatrix.setValue(12U, mFloatArray.at(3U));
	newMatrix.setValue(13U, mFloatArray.at(7U));
	newMatrix.setValue(14U, mFloatArray.at(11U));
	newMatrix.setValue(15U, mFloatArray.at(15U));
	return newMatrix;
}

AEMatrix4x4 AEMatrix4x4::inverseTranspose() const
{
	AEMatrix4x4 newMatrix;
	AEFixedArray<AEFloat32> temp(12U);	// These are temporary storages for the cofactors
	AEFloat32 determinant(0.0F);				// Determinant value.

	// Setup the cofactors for the first 8.
	temp.at(0U) = mFloatArray.at(10U) * mFloatArray.at(15U);
	temp.at(1U) = mFloatArray.at(11U) * mFloatArray.at(14U);
	temp.at(2U) = mFloatArray.at(9U) * mFloatArray.at(15U);
	temp.at(3U) = mFloatArray.at(11U) * mFloatArray.at(13U);
	temp.at(4U) = mFloatArray.at(9U) * mFloatArray.at(14U);
	temp.at(5U) = mFloatArray.at(10U) * mFloatArray.at(13U);
	temp.at(6U) = mFloatArray.at(8U) * mFloatArray.at(15U);
	temp.at(7U) = mFloatArray.at(11U) * mFloatArray.at(12U);
	temp.at(8U) = mFloatArray.at(8U) * mFloatArray.at(14U);
	temp.at(9U) = mFloatArray.at(10U) * mFloatArray.at(12U);
	temp.at(10U) = mFloatArray.at(8U) * mFloatArray.at(13U);
	temp.at(11U) = mFloatArray.at(9U) * mFloatArray.at(12U);
	// Setup the first 8 elements of the new matrix.
	newMatrix.setValue(0U, (temp.at(0U) * mFloatArray.at(5U)) + (temp.at(3U) * mFloatArray.at(6U)) + (temp.at(4U) * mFloatArray.at(7U))
			- (temp.at(1U) * mFloatArray.at(5U)) - (temp.at(2U) * mFloatArray.at(6U)) - (temp.at(5U) * mFloatArray.at(7U)));
	newMatrix.setValue(1U, (temp.at(1U) * mFloatArray.at(4U)) + (temp.at(6U) * mFloatArray.at(6U)) + (temp.at(9U) * mFloatArray.at(7U))
			- (temp.at(0U) * mFloatArray.at(4U)) - (temp.at(7U) * mFloatArray.at(6U)) - (temp.at(8U) * mFloatArray.at(7U)));
	newMatrix.setValue(2U,	(temp.at(2U) * mFloatArray.at(4U)) + (temp.at(7U) * mFloatArray.at(5U)) + (temp.at(10U) * mFloatArray.at(7U))
			- (temp.at(3U) * mFloatArray.at(4U)) - (temp.at(6U) * mFloatArray.at(5U)) - (temp.at(11U) * mFloatArray.at(7U)));
	newMatrix.setValue(3U, (temp.at(5U) * mFloatArray.at(4U)) + (temp.at(8U) * mFloatArray.at(5U)) + (temp.at(11U) * mFloatArray.at(6U))
			- (temp.at(4U) * mFloatArray.at(4U)) - (temp.at(9U) * mFloatArray.at(5U)) - (temp.at(10U) * mFloatArray.at(6U)));
	newMatrix.setValue(4U, (temp.at(1U) * mFloatArray.at(1U)) + (temp.at(2U) * mFloatArray.at(2U)) + (temp.at(5U) * mFloatArray.at(3U))
			- (temp.at(0U) * mFloatArray.at(1U)) - (temp.at(3U) * mFloatArray.at(2U)) - (temp.at(4U) * mFloatArray.at(3U)));
	newMatrix.setValue(5U,	(temp.at(0U) * mFloatArray.at(0U)) + (temp.at(7U) * mFloatArray.at(2U)) + (temp.at(8U) * mFloatArray.at(3U))
			- (temp.at(1U) * mFloatArray.at(0U)) - (temp.at(6U) * mFloatArray.at(2U)) - (temp.at(9U) * mFloatArray.at(3U)));
	newMatrix.setValue(6U,	(temp.at(3U) * mFloatArray.at(0U)) + (temp.at(6U) * mFloatArray.at(1U)) + (temp.at(11U) * mFloatArray.at(3U))
			- (temp.at(2U) * mFloatArray.at(0U)) - (temp.at(7U) * mFloatArray.at(1U)) - (temp.at(10U) * mFloatArray.at(3U)));
	newMatrix.setValue(7U, (temp.at(4U) * mFloatArray.at(0U)) + (temp.at(9U) * mFloatArray.at(1U)) + (temp.at(10U) * mFloatArray.at(2U))
			- (temp.at(5U) * mFloatArray.at(0U)) - (temp.at(8U) * mFloatArray.at(1U)) - (temp.at(11U) * mFloatArray.at(2U)));


	// Setup the cofactors for the last 8 values.
	temp.at(0U) = mFloatArray.at(2U) * mFloatArray.at(7U);
	temp.at(1U) = mFloatArray.at(3U) * mFloatArray.at(6U);
	temp.at(2U) = mFloatArray.at(1U) * mFloatArray.at(7U);
	temp.at(3U) = mFloatArray.at(3U) * mFloatArray.at(5U);
	temp.at(4U) = mFloatArray.at(1U) * mFloatArray.at(6U);
	temp.at(5U) = mFloatArray.at(2U) * mFloatArray.at(5U);
	temp.at(6U) = mFloatArray.at(0U) * mFloatArray.at(7U);
	temp.at(7U) = mFloatArray.at(3U) * mFloatArray.at(4U);
	temp.at(8U) = mFloatArray.at(0U) * mFloatArray.at(6U);
	temp.at(9U) = mFloatArray.at(2U) * mFloatArray.at(4U);
	temp.at(10U) = mFloatArray.at(0U) * mFloatArray.at(5U);
	temp.at(11U) = mFloatArray.at(1U) * mFloatArray.at(4U);

	// Update the last 8 cofactors.
	newMatrix.setValue(8U,	temp.at(0U) * mFloatArray.at(13U)  + temp.at(3U) * mFloatArray.at(14U)  + temp.at(4U) * mFloatArray.at(15U) 
			- temp.at(1U) * mFloatArray.at(13U)  - temp.at(2U) * mFloatArray.at(14U)  - temp.at(5U) * mFloatArray.at(15U));
	newMatrix.setValue(9U,	temp.at(1U) * mFloatArray.at(12U)  + temp.at(6U) * mFloatArray.at(14U)  + temp.at(9U) * mFloatArray.at(15U) 
			- temp.at(0U) * mFloatArray.at(12U)  - temp.at(7U) * mFloatArray.at(14U)  - temp.at(8U) * mFloatArray.at(15U));
	newMatrix.setValue(10U,	temp.at(2U) * mFloatArray.at(12U)  + temp.at(7U) * mFloatArray.at(13U)  + temp.at(10U) * mFloatArray.at(15U) 
			- temp.at(3U) * mFloatArray.at(12U)  - temp.at(6U) * mFloatArray.at(13U)  - temp.at(11U) * mFloatArray.at(15U));
	newMatrix.setValue(11U,	temp.at(5U) * mFloatArray.at(12U)  + temp.at(8U) * mFloatArray.at(13U)  + temp.at(11U) * mFloatArray.at(14U) 
			- temp.at(4U) * mFloatArray.at(12U)  - temp.at(9U) * mFloatArray.at(13U)  - temp.at(10U) * mFloatArray.at(14U));
	newMatrix.setValue(12U,	temp.at(2U) * mFloatArray.at(10U)  + temp.at(5U) * mFloatArray.at(11U)  + temp.at(1U) * mFloatArray.at(9U) 
			- temp.at(4U) * mFloatArray.at(11U)  - temp.at(0U) * mFloatArray.at(9U)  - temp.at(3U) * mFloatArray.at(10U));
	newMatrix.setValue(13U,	temp.at(8U) * mFloatArray.at(11U)  + temp.at(0U) * mFloatArray.at(8U)  + temp.at(7U) * mFloatArray.at(10U) 
			- temp.at(6U) * mFloatArray.at(10U)  - temp.at(9U) * mFloatArray.at(11U)  - temp.at(1U) * mFloatArray.at(8U));
	newMatrix.setValue(14U,	temp.at(6U) * mFloatArray.at(9U)  + temp.at(11U) * mFloatArray.at(11U)  + temp.at(3U) * mFloatArray.at(8U) 
			- temp.at(10U) * mFloatArray.at(11U)  - temp.at(2U) * mFloatArray.at(8U)  - temp.at(7U) * mFloatArray.at(9U));
	newMatrix.setValue(15U,	temp.at(10U) * mFloatArray.at(10U)  + temp.at(4U) * mFloatArray.at(8U)  + temp.at(9U) * mFloatArray.at(9U) 
			- temp.at(8U) * mFloatArray.at(9U)  - temp.at(11U) * mFloatArray.at(10U)  - temp.at(5U) * mFloatArray.at(8U));

	determinant	= mFloatArray.at(0U) * newMatrix.getValue(0U) + mFloatArray.at(1U) * newMatrix.getValue(1U) + mFloatArray.at(2U) * newMatrix.getValue(2U) + mFloatArray.at(3U) * newMatrix.getValue(3U);
	if (determinant == 0.0F)
	{
		AEMatrix4x4 identity;
		identity.setToIdentity();
		return identity;
	}

	newMatrix = newMatrix / determinant;

	return newMatrix;
}

bool AEMatrix4x4::operator==(const AEMatrix4x4& rMatrix)
{
	AEUint valueIndex(0U);
	for (AEFixedArray<AEFloat32>::constIterator itrValue(mFloatArray.begin()); itrValue != mFloatArray.end(); ++itrValue)
	{
		if ((*itrValue) != rMatrix.getValue(valueIndex))
			return false;

		++valueIndex;
	}
	
	return true;
}

bool AEMatrix4x4::operator!=(const AEMatrix4x4& rMatrix)
{
	return !((*this) == rMatrix);
}

void AEMatrix4x4::operator+=(const AEMatrix4x4& rMatrix)
{
	(*this) = (*this) + rMatrix;
}

void AEMatrix4x4::operator-=(const AEMatrix4x4& rMatrix)
{
	(*this) = (*this) - rMatrix;
}

void AEMatrix4x4::operator*=(const AEMatrix4x4& rMatrix)
{
	(*this) = (*this) * rMatrix;
}

void AEMatrix4x4::operator*=(const AEFloat32 rValue)
{
	(*this) = (*this) * rValue;
}

void AEMatrix4x4::operator/=(const AEFloat32 rValue)
{
	(*this) = (*this) / rValue;
}

}

