#ifndef AE_VECTOR_3_
#define AE_VECTOR_3_

#include "AESystemTypes.h"

namespace AE 
{

//! float32 3d vector class.
class AEVector3
{
	public:
		AEVector3() : mX(0.0F) , mY(0.0F), mZ(0.0F) {}
		AEVector3(const AEFloat32 xValue, const AEFloat32 yValue, const AEFloat32 zValue) : mX(xValue), mY(yValue), mZ(zValue) {}
		AEVector3(const AEVector3& vector3) : mX(vector3.x()), mY(vector3.y()), mZ(vector3.z()) {}

		AEFloat32 x() const { return mX; }
		AEFloat32 y() const { return mY; }
		AEFloat32 z() const { return mZ; }

		void setX(const AEFloat32 value) { mX = value; }
		void setY(const AEFloat32 value) { mY = value; }
		void setZ(const AEFloat32 value) { mZ = value; }

		void setXYZ	(const AEFloat32 x, const AEFloat32 y, const AEFloat32 z) { mX = x; mY = y; mZ = z; }
		void setAll(const AEFloat32 value) { mX = mY = mZ = value; }

		void normalize();
		AEVector3 getNormalized() const;
		AEFloat32 length() const { return static_cast<AEFloat32>(sqrt((mX * mX) + (mY * mY) + (mZ * mZ))); }

		AEVector3 crossProduct(const AEVector3& rVector) const
		{ return AEVector3((mY * rVector.z()) - (mZ * rVector.y()), (mZ * rVector.x()) - (mX * rVector.z()), (mX * rVector.y()) - (mY * rVector.x())); }

		AEFloat32 dotProduct(const AEVector3& rVector) const
		{ return (mX * rVector.x()) + (mY * rVector.y()) + (mZ * rVector.z()); }

		//! Simple yet very useful linear interpolation to another vector.
		AEVector3 lerp(const AEVector3& vector2, const AEFloat32 factor) const { return (*this) * (1.0F - factor) + (vector2 * factor); }

		//! Quadratic interpolation.
		AEVector3 qerp(const AEVector3& vector2, const AEVector3& vector3, const AEFloat32 factor)
		{ return (*this) * (1.0F - factor) * (1.0F - factor) + 2.0F * vector2 * factor * (1.0F - factor) + vector3 * factor * factor; }

		// Binary operators
		AEVector3 operator+(const AEVector3& rVector) const { return AEVector3(mX + rVector.x(), mY + rVector.y(), mZ + rVector.z()); }
		AEVector3 operator-(const AEVector3& rVector) const { return AEVector3(mX - rVector.x(), mY - rVector.y(), mZ - rVector.z()); }
		AEVector3 operator*(const AEFloat32& value) const { return AEVector3(mX * value, mY * value, mZ * value); }
		AEVector3 operator/(const AEFloat32& value) const { return (value == 0.0F) ? AEVector3::ZERO : AEVector3(mX / value, mY / value, mZ / value); }

		friend AEVector3 operator*(const AEFloat32& value, const AEVector3& rVector) { return rVector * value; }

		bool operator==(const AEVector3& rVector) const { return ((mX == rVector.x()) && (mY == rVector.y()) && (mZ == rVector.z())) ? true : false; }
		bool operator!=(const AEVector3& rVector) const { return !((*this) == rVector); }
		void operator+=(const AEVector3& rVector) { mX += rVector.x(); mY += rVector.y(); mZ += rVector.z(); }
		void operator-=(const AEVector3& rVector) { mX -= rVector.x(); mY -= rVector.y(); mZ -= rVector.z(); }
		void operator*=(const AEFloat32 value) { mX *= value; mY *= value; mZ *= value; }
		void operator/=(const AEFloat32 value) { if (value == 0.0F) return; else mX /= value; mY /= value; mZ /= value; }
		
		void xRotate(const AEFloat64 radians) { (*this) = getXRotated(radians); }
		void yRotate(const AEFloat64 radians) { (*this) = getYRotated(radians); }
		void zRotate(const AEFloat64 radians) { (*this) = getZRotated(radians); }

		// Unary operators (switches values)
		AEVector3 operator-(void) const { return AEVector3(-mX, -mY, -mZ); }
		AEVector3 operator+(void) const { return *this; }

		AEVector3 getXRotated(const AEFloat64 radians) const;
		AEVector3 getYRotated(const AEFloat64 radians) const;
		AEVector3 getZRotated(const AEFloat64 radians) const;
		
		//! Refer to a pointer
		operator AEFloat32* () { return &mX; }
		//! Const version.
		operator const AEFloat32* () { return &mX; }

		static const AEVector3 ZERO;
		static const AEVector3 UNIT_X;
		static const AEVector3 UNIT_Y;
		static const AEVector3 UNIT_Z;

	private:
		AEFloat32 mX;
		AEFloat32 mY;
		AEFloat32 mZ;
};

} // namespace AE

#endif // AE_VECTOR_3_

