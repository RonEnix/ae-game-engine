#include "AEStringFunctions.h"

namespace AE
{

const AEString intToString(const int value, const int baseValue)
{
	if (baseValue < 2 || baseValue > 36)
		return AEString();

	AEInt number(value);
	AEString returnString;
	bool numberNegative(false);
	if (number < 0)
	{
		numberNegative = true;
		number = -number;
	}

	int currentValue;
	while (number > 0)
	{
		currentValue = number % baseValue;
		if (currentValue < 9)
			returnString.insert(returnString.begin(), static_cast<char>(currentValue + '0'));
		else
			returnString += static_cast<char>(currentValue - 10 + 'A');

		number /= baseValue;
	}

	if (numberNegative) 
		returnString.insert(returnString.begin(), '-');

	return returnString;
}

const AEString intToString(const long value, const int baseValue)
{
	if (baseValue < 2 || baseValue > 36)
		return AEString();

	AEInt number(value);
	AEString returnString;
	bool numberNegative(false);
	if (number < 0)
	{
		numberNegative = true;
		number = -number;
	}

	int currentValue;
	while (number > 0)
	{
		currentValue = number % baseValue;
		if (currentValue < 9)
			returnString.insert(returnString.begin(), static_cast<char>(currentValue + '0'));
		else
			returnString += static_cast<char>(currentValue - 10 + 'A');

		number /= baseValue;
	}

	if (numberNegative) 
		returnString.insert(returnString.begin(), '-');

	return returnString;
}

const AEString unsignedIntToString(const unsigned int value, const int baseValue)
{
	if (baseValue < 2 || baseValue > 36)
		return AEString();

	unsigned int number(value);
	AEString returnString;

	unsigned int currentValue;
	while (number > 0)
	{
		currentValue = number % baseValue;
		if (currentValue < 9)
			returnString.insert(returnString.begin(), static_cast<char>(currentValue + '0'));
		else
			returnString += static_cast<char>(currentValue - 10 + 'A');

		number /= baseValue;
	}

	return returnString;
}

}

