#ifndef AE_ARRAY_H_
#define AE_ARRAY_H_

#include <new>
#include <memory>

#include "AESystemTypes.h"
#include "AELogger.h"

namespace AE
{

//! Logger hasn't been placed in as well as assert code. These are a TODO
//! Static sized array with push and pop back feature of a stl vector.
template<typename T>
class AEFixedArray
{
	public:
		AEFixedArray() : mFirst(NULL) , mEnd(NULL) , mSize(0U) , mCapacity(0U) {}
		~AEFixedArray();

		AEFixedArray(const AEUint capacity);
		AEFixedArray(const AEFixedArray<T>& rArray);
		AEFixedArray<T>& operator=(const AEFixedArray<T>& rArray);

		void pushBack(const T& element);
		void popBack();

		AEUint size() const { return mSize; }
		AEUint capacity() const { return mCapacity; }

		//! Clears everything in the array to its original constructor state.
		void clear();

		//! Removes elements in the array but keeps the capacity and the memory so the array can used.
		void reset();

		T& at(const AEUint position);
		const T& at(const AEUint position) const;

		typedef T* iterator;
		typedef const T* constIterator;

		//! Allow T* typePointer = AEFixedArray<T>(); So we can do the following: iterator startOfArray(exisitingArray);
		operator T* () { return mFirst; }
		//! Const version.
		operator const T* () { return mFirst; }

		T* begin() { return mFirst; }
		const T* begin() const { return begin; }

		T* end() { return mEnd; }
		const T* end() const { return mEnd; }

	private:
		T* mFirst;
		T* mEnd;

		AEUint mSize;		//!: Current number of elements in the array.
		AEUint mCapacity;	//!: Number of elements this array and hold.

};

template<typename T>
AEFixedArray<T>::~AEFixedArray()
{
	// Call destructor on containing elements first.
	for (iterator itrElement(mFirst); itrElement != mEnd; ++itrElement)
	{
		itrElement->T::~T();
	}
	delete mFirst;
}

template<typename T>
AEFixedArray<T>::AEFixedArray(const AEUint capacity)
: mFirst(NULL)
, mEnd(NULL)
, mSize(0U)
, mCapacity(capacity)
{
	if (mCapacity > 0U)
	{
		// Create the buffer of type Ts for placements news.
		mFirst = reinterpret_cast<T*>(operator new (sizeof(T) * mCapacity));
		if (!mFirst)
		{
			// Ran out of memory. Kill application.
			AELOG(LOG_ERROR) << "[FIXED ARRAY] No memory available for fixed size array." << ENDL;
		}
		else
		{
			mEnd = mFirst;
		}
	}
}

template<typename T>
AEFixedArray<T>::AEFixedArray(const AEFixedArray<T>& rArray)
: mFirst(NULL)
, mEnd(NULL)
, mSize(0U)
, mCapacity(0U)
{
	// Copy contents of an array.
	// Does not copy up to the arrays capacity. Will only copy its active contents.
	const AEUint inArraySize(rArray.mSize);
	mCapacity = mSize = inArraySize;

	if (inArraySize == 0U)
	{
		AELOG(LOG_WARNING) << "[FIXED ARRAY] Incoming fixed array is empty." << ENDL;
		return;
	}

	mFirst = reinterpret_cast<T*>(operator new (sizeof(T) * inArraySize));
	if (!mFirst)
	{
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Not enough memory available for array." << ENDL;
		return;
	}

	mEnd = mFirst + mSize;

	// Copy into new array
	T* inArrayFirst(rArray.mFirst);
	for (iterator itrElement(mFirst); itrElement != mEnd; ++itrElement)
	{
		new (itrElement) T(*inArrayFirst);
		++inArrayFirst;
	}

}

template<typename T>
AEFixedArray<T>& AEFixedArray<T>::operator=(const AEFixedArray<T>& rArray)
{
	const AEUint inArraySize(rArray.mSize);
	mCapacity = mSize = inArraySize;

	if (inArraySize == 0U)
	{
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Incoming fixed array is empty." << ENDL;
		clear();
		return *this;
	}

	mFirst = reinterpret_cast<T*>(operator new (sizeof(T) * inArraySize));
	if (!mFirst)
	{
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Not enough memory available for array." << ENDL;
		clear();
		return *this;
	}

	mEnd = mFirst + mSize;

	// Copy into new array
	T* inArrayFirst(rArray.mFirst);
	for (iterator itrElement(mFirst); itrElement != mEnd; ++itrElement)
	{
		new (itrElement) T(*inArrayFirst);
		++inArrayFirst;
	}

	return *this;
}

template<typename T>
void AEFixedArray<T>::pushBack(const T& element)
{
	if (mSize >= mCapacity)
		AELOG(LOG_WARNING) << "[FIXED ARRAY] Fixed array full. Capacity at: " << mCapacity;
	else
	{
		// Placement new on the last end position:
		new (mEnd) T(element);
		++mEnd;		// Move to new available T space.
		++mSize; 
	}
}

template<typename T>
void AEFixedArray<T>::popBack()
{
	if (mFirst == mEnd)
		return;

	// mEnd is the current empty unused and readied space so move back to the previous and clear that instead.
	--mEnd;
	mEnd->~T();
}

template<typename T>
void AEFixedArray<T>::clear()
{
	for (iterator itrElement(mFirst); itrElement != mEnd; ++itrElement)
		itrElement->~T();

	mSize = 0U;
	mCapacity = 0U;
	mEnd = NULL;
	delete mFirst;
}

template<typename T>
void AEFixedArray<T>::reset()
{
	for (iterator itrElement(mFirst); itrElement != mEnd; ++itrElement)
		itrElement->~T();

	mSize = 0U;
	mEnd = mFirst;
}

template<typename T>
T& AEFixedArray<T>::at(const AEUint position)
{
	if (mSize == 0U)
	{
		// Fatal log. Terminate with error.
		// No fixed array content.
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Fixed array has no elements.";
	}

	if (position >= mSize)
	{
		// Fatal log. Out of bounds.
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Accessing invalid memory out of array bounds.";
	}

	return *(mFirst + position);
}

template<typename T>
const T& AEFixedArray<T>::at(const AEUint position) const
{
	if (mSize == 0U)
	{
		// Fatal log. Terminate with error.
		// No fixed array content.
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Fixed array has no elements.";
	}

	if (position >= mSize)
	{
		// Fatal log. Out of bounds.
		AELOG(LOG_ERROR) << "[FIXED ARRAY] Accessing invalid memory out of array bounds.";
	}

	return *(mFirst + position);
}

}	// namespace AE

#endif // AE_ARRAY_H_

