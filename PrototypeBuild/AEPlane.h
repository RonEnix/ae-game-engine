#ifndef AE_PLANE_H_
#define AE_PLANE_H_

#include "AESystemTypes.h"

namespace AE
{

class AEPlane
{
	public:
		enum IntersectStatus
		{
			  POINT_ON_PLANE = 0
			, POINT_FRONT_OF_PLANE
			, POINT_BEHIND_PLANE
		};

		AEPlane()
		: mNormal(AEVector3::ZERO)
		, mDistance(0.0F)
		{}

		AEPlane(const AEVector3& normal, const AEFloat32 distance)
		: mNormal(normal)
		, mDistance(distance)
		{}

		AEPlane(const AEPlane& plane)
		: mNormal(plane.mNormal)
		, mDistance(plane.mDistance)
		{}

		~AEPlane() {}

		// Basic operators
		bool operator==(const AEPlane& rPlane) const;
		bool operator!=(const AEPlane& rPlane) const;
		// Unary operators
		AEPlane operator-(void) const { return AEPlane(-mNormal, -mDistance); }
		AEPlane operator+(void) const { return (*this); }
		
		void setNormal(const AEVector3& normal) { mNormal = normal; }
		void setDistance(const AEFloat32 distance) { mDistance = distance; }
		void generateFromPoints(const AEVector3& point1, const AEVector3& point2, const AEVector3& point3);
		void normalize()
		{
			const AEFloat32 length(mNormal.length());
			mNormal /= length;
			mDistance /= length;
		}

		IntersectStatus checkPointIntersect(const AEVector3& point) const;

		AEFloat32 getDistance() const { return mDistance; }
		const AEVector3& getNormal() const { return mNormal; }

	private:
		AEVector3 mNormal;
		AEFloat32 mDistance;
};

}

#endif // AE_PLANE_H_

