#if defined(LINUX)
#include <unistd.h>
#include <SOIL.h>
#include <string.h>

#include "AESystemTypes.h"
#include "AERenderSystem.h"
#include "AEBitMapFont.h"
#include "AEArray.h"
#include "AEMaterial.h"
#include "AEStringFunctions.h"
#include "AEPlane.h"
#include "AEMatrix4x4.h"

using namespace AE;
using namespace std;

// All const all have upper cases.
const AEInt32 SCREEN_WIDTH(800U);
const AEInt32 SCREEN_HEIGHT(480U);
const AEString TITLE("Affinity Engine OpenGL Render System");

// Future reference: Some maths libraries you could use: CML at cmldev.net

#define SHADER_TEST
//#define FIXED_PIPELINE
#define CUSTOM_MEMORY_MANAGER_TEST

void drawScene();
void drawTriangle(const AEFloat32 posX, const AEFloat32 posY, const AEFloat32 posZ, const AEFloat32 width, const AEFloat32 height);
void drawPyramid(const AEFloat32 width, const AEFloat32 height);
void drawCube(const AEFloat32 width, const AEFloat32 height, const AEFloat32 depth, const AEFloat32 rotation);
void drawGizmo(const AEFloat32 xPos, const AEFloat32 yPos, const AEFloat32 zPos);
void drawGrid(const AEUint32 gridSquareSize, const AEFloat32 gridSpace);

GLuint testTexture;

void drawScene()
{
	gluLookAt(0, 0, 6, 0, 0, -6, 0, 1, 0);
	glTranslatef(-1.5F, 0.0F, -6.0F);

	glBegin(GL_TRIANGLES);
		glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
		glVertex3f(0.0F, 1.0F, 0.0F);		// Top point.
		glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
		glVertex3f(-1.0F, -1.0F, 0.0F);		// Bottom left point.
		glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
		glVertex3f(1.0F, -1.0F, 0.0F);		// Bottom right point.
	glEnd();

	glColor4f(0.5F, 0.0F, 0.05, 1.0F);
	glTranslatef(3.0F, 0.0F, 0.0F);
	glBegin(GL_QUADS);
		glVertex3f(-1.0F, 1.0F, 0.0F);		// Top left point.
		glVertex3f(1.0F, 1.0F, 0.0F);		// Top right point.
		glVertex3f(1.0F, -1.0F, 0.0F);		// Bottom right point.
		glVertex3f(-1.0F, -1.0F, 0.0F);		// Bottom left point.
	glEnd();
}

void drawTriangle(const AEFloat32 posX, const AEFloat32 posY, const AEFloat32 posZ, const AEFloat32 width, const AEFloat32 height)
{
	glBegin(GL_TRIANGLES);
		// Winding is anti-clockwise and backface culling is enabled.
		glNormal3f(1.0F, 0.0F, 0.0F);
		glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
		glVertex3f(posX, posY + (height * 0.5F), posZ);							// Top point.
		glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
		glVertex3f(posX - (width * 0.5F), posY - (height * 0.5F), posZ);		// Bottom left point.
		glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
		glVertex3f(posX + (width * 0.5F), posY - (height * 0.5F), posZ);		// Bottom right point.
	glEnd();
}

void drawPyramid(const AEFloat32 width, const AEFloat32 height)
{
	const float halfWidth(width * 0.5F);
	const float halfHeight(height * 0.5F);
	
	glBegin(GL_TRIANGLES);
		glVertex3f(0.0F, halfHeight, 0.0F);					// Front top point
		glVertex3f(-halfWidth, -halfHeight, halfWidth);		// Front bottom left point
		glVertex3f(halfWidth, -halfHeight, halfWidth);		// Front bottom right point
		glVertex3f(0.0F, halfHeight, 0.0F);					// Right top point
		glVertex3f(halfWidth, -halfHeight, halfWidth);		// Right bottom left point
		glVertex3f(halfWidth, -halfHeight, -halfWidth);		// Right bottom right point
		glVertex3f(0.0F, halfHeight, 0.0F);					// Back top point
		glVertex3f(halfWidth, -halfHeight, -halfWidth);		// Back bottom left point
		glVertex3f(-halfWidth, -halfHeight, -halfWidth);	// Back bottom right point
		glVertex3f(0.0F, halfHeight, 0.0F);					// Left top point
		glVertex3f(-halfWidth, -halfHeight, -halfWidth);	// Left bottom left point
		glVertex3f(-halfWidth, -halfHeight, halfWidth);		// Left bottom right point
		// Cube bottom face:
	glEnd();
	glBegin(GL_QUADS);
		glVertex3f(-halfWidth, -halfHeight, -halfWidth);	// Bottom-Left
		glVertex3f(halfWidth, -halfHeight, -halfWidth);		// Bottom-Right
		glVertex3f(halfWidth, -halfHeight, halfWidth);		// Top-Right
		glVertex3f(-halfWidth, -halfHeight, halfWidth);		// Top-Left
	glEnd();
}

void drawCube(const AEFloat32 width, const AEFloat32 height, const AEFloat32 depth, const AEFloat32 rotation)
{
	const float halfWidth(width * 0.5F);
	const float halfHeight(height * 0.5F);
	const float halfDepth(depth * 0.5F);

	glRotatef(rotation, 0.0F, 1.0F, 0.0F);
	glBindTexture(GL_TEXTURE_2D, testTexture);		// Remember UV of 0,0 is bottom left.
	glBegin(GL_QUADS);
		// Cube front face:
		glColor4f(0.1F, 0.8F, 0.6F, 1.0F);
		glNormal3f(0.0F, 0.0F, 1.0F);
		//glNormal3f(-1.0F, -1.0F, 1.0F);
		glTexCoord2f(0.0F, 0.0F);
		glVertex3f(-halfWidth, -halfHeight, halfDepth);		// Bottom-Left
		//glNormal3f(1.0F, -1.0F, 1.0F);
		glTexCoord2f(1.0F, 0.0F);
		glVertex3f(halfWidth, -halfHeight, halfDepth);		// Bottom-Right
		//glNormal3f(1.0F, 1.0F, 1.0F);
		glTexCoord2f(1.0F, 1.0F);
		glVertex3f(halfWidth, halfHeight, halfDepth);		// Top-Right
		//glNormal3f(-1.0F, 1.0F, 1.0F);
		glTexCoord2f(0.0F, 1.0F);
		glVertex3f(-halfWidth, halfHeight, halfDepth);		// Top-Left
		//glBindTexture(GL_TEXTURE_2D, 0);		// Remember UV of 0,0 is bottom left.
		// Cube right face:
		glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
		glNormal3f(1.0F, 0.0F, 0.0F);
		//glNormal3f(1.0F, 0.0F, 1.0F);
		glTexCoord2f(0.0F, 0.0F);
		glVertex3f(halfWidth, -halfHeight, halfDepth);		// Bottom-Left
		//glNormal3f(1.0F, 0.0F, -1.0F);
		glTexCoord2f(1.0F, 0.0F);
		glVertex3f(halfWidth, -halfHeight, -halfDepth);		// Bottom-Right
		//glNormal3f(1.0F, 0.0F, -1.0F);
		glTexCoord2f(1.0F, 1.0F);
		glVertex3f(halfWidth, halfHeight, -halfDepth);		// Top-Right
		//glNormal3f(1.0F, 0.0F, 1.0F);
		glTexCoord2f(0.0F, 1.0F);
		glVertex3f(halfWidth, halfHeight, halfDepth);		// Top-Left
		// Cube back face:
		//glColor4f(0.0F, 1.0F, 1.0F, 1.0F);
		glNormal3f(0.0F, 0.0F, -1.0F);
		//glNormal3f(1.0F, 0.0F, -1.0F);
		glTexCoord2f(0.0F, 0.0F);
		glVertex3f(halfWidth, -halfHeight, -halfDepth);		// Bottom-Left
		//glNormal3f(-1.0F, 0.0F, -1.0F);
		glTexCoord2f(1.0F, 0.0F);
		glVertex3f(-halfWidth, -halfHeight, -halfDepth);	// Bottom-Right
		//glNormal3f(-1.0F, 0.0F, -1.0F);
		glTexCoord2f(1.0F, 1.0F);
		glVertex3f(-halfWidth, halfHeight, -halfDepth);		// Top-Right
		//glNormal3f(1.0F, 0.0F, -1.0F);
		glTexCoord2f(0.0F, 1.0F);
		glVertex3f(halfWidth, halfHeight, -halfDepth);		// Top-Left
		// Cube left face:
		//glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
		glNormal3f(-1.0F, 0.0F, 0.0F);
		//glNormal3f(-1.0F, 0.0F, -1.0F);
		glTexCoord2f(0.0F, 0.0F);
		glVertex3f(-halfWidth, -halfHeight, -halfDepth);	// Bottom-Left
		//glNormal3f(-1.0F, 0.0F, 1.0F);
		glTexCoord2f(1.0F, 0.0F);
		glVertex3f(-halfWidth, -halfHeight, halfDepth);		// Bottom-Right
		//glNormal3f(-1.0F, 0.0F, 1.0F);
		glTexCoord2f(1.0F, 1.0F);
		glVertex3f(-halfWidth, halfHeight, halfDepth);		// Top-Right
		//glNormal3f(-1.0F, 0.0F, -1.0F);
		glTexCoord2f(0.0F, 1.0F);
		glVertex3f(-halfWidth, halfHeight, -halfDepth);		// Top-Left
		// Cube top face:
		//glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
		glNormal3f(0.0F, 1.0F, 0.0F);
		//glNormal3f(-1.0F, 1.0F, 1.0F);
		glTexCoord2f(0.0F, 0.0F);
		glVertex3f(-halfWidth, halfHeight, halfDepth);		// Bottom-Left
		//glNormal3f(1.0F, 1.0F, 1.0F);
		glTexCoord2f(1.0F, 0.0F);
		glVertex3f(halfWidth, halfHeight, halfDepth);		// Bottom-Right
		//glNormal3f(1.0F, 1.0F, -1.0F);
		glTexCoord2f(1.0F, 1.0F);
		glVertex3f(halfWidth, halfHeight, -halfDepth);		// Top-Right
		//glNormal3f(-1.0F, 1.0F, -1.0F);
		glTexCoord2f(0.0F, 1.0F);
		glVertex3f(-halfWidth, halfHeight, -halfDepth);		// Top-Left
		// Cube bottom face:
		//glColor4f(1.0F, 1.0F, 1.0F, 0.5F);
		glNormal3f(0.0F, 1.0F, 0.0F);
		glTexCoord2f(0.0F, 0.0F);
		glVertex3f(-halfWidth, -halfHeight, halfDepth);		// Bottom-Left
		glTexCoord2f(1.0F, 0.0F);
		glVertex3f(halfWidth, -halfHeight, halfDepth);		// Bottom-Right
		glTexCoord2f(1.0F, 1.0F);
		glVertex3f(halfWidth, -halfHeight, -halfDepth);		// Top-Right
		glTexCoord2f(0.0F, 1.0F);
		glVertex3f(-halfWidth,- halfHeight, -halfDepth);	// Top-Left
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void drawGizmo(const AEFloat32 xPos, const AEFloat32 yPos, const AEFloat32 zPos)
{
	glDisable(GL_LIGHTING);
	glLineWidth(2.0F);
	const AEFloat32 pyramidWidth(0.15F);
	const AEFloat32 pyramidHeight(0.4F);
	const AEFloat32 lineLength(1.4F);

	glPushMatrix();
		glTranslatef(xPos, yPos, zPos);
		glPushMatrix();
			glColor4f(0.6F, 0.7F, 0.2F, 1.0F);	// X line
			glBegin(GL_LINES);
				glVertex3f(0.0F, 0.0F, 0.0F);
				glVertex3f(lineLength, 0.0F, 0.0F);
			glEnd();
			glTranslatef(lineLength, 0.0F, 0.0F);
			glRotatef(-90.0F, 0.0, 0.0, 1.0F);
			glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
			drawPyramid(pyramidWidth, pyramidHeight);
		glPopMatrix();

		glPushMatrix();
			glColor4f(0.6F, 0.7F, 0.2F, 1.0F);	// Y line
			glBegin(GL_LINES);
				glVertex3f(0.0F, 0.0F, 0.0F);
				glVertex3f(0.0F, lineLength, 0.0F);
			glEnd();
			glTranslatef(0.0F, lineLength, 0.0F);
			glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
			drawPyramid(pyramidWidth, pyramidHeight);
		glPopMatrix();

		glPushMatrix();
			glColor4f(0.6F, 0.7F, 0.2F, 1.0F);	// Z line
			glBegin(GL_LINES);
				glVertex3f(0.0F, 0.0F, 0.0F);
				glVertex3f(0.0F, 0.0F, lineLength);
			glEnd();
			glTranslatef(0.0F, 0.0F, lineLength);
			glRotatef(90.0F, 1.0, 0.0, 0.0F);
			glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
			drawPyramid(pyramidWidth, pyramidHeight);
		glPopMatrix();
	glPopMatrix();

	glLineWidth(1.0F);
	glEnable(GL_LIGHTING);
}

// Draws grid with center position of 0, 0, 0
void drawGrid(const AEUint32 gridSquareSize, const AEFloat32 gridSpace)
{
	const AEFloat32 halfLineCount(gridSquareSize * 0.5F);

	glDisable(GL_LIGHTING);
	glColor4f(0.1F, 0.1F, 0.1F, 1.0F);
	glPushMatrix();
		for (AEFloat32 lineIndex(-halfLineCount); lineIndex <= halfLineCount; lineIndex += gridSpace)
		{
			glBegin(GL_LINES);
				// Horizontal line
				glVertex3f(static_cast<AEFloat32>(-halfLineCount), 0.0F, static_cast<AEFloat32>(lineIndex));
				glVertex3f(static_cast<AEFloat32>(halfLineCount), 0.0F, static_cast<AEFloat32>(lineIndex));
				// Vertical line
				glVertex3f(static_cast<AEFloat32>(lineIndex), 0.0F, static_cast<AEFloat32>(-halfLineCount));
				glVertex3f(static_cast<AEFloat32>(lineIndex), 0.0F, static_cast<AEFloat32>(halfLineCount));
			glEnd();
		}
	glPopMatrix();
	//glEnable(GL_LIGHTING);
}

#if 0
class TestClass
{
	public:
		~TestClass()
		{
			printf("class destroyed\n");
		}
};
#endif

#ifdef CUSTOM_MEMORY_MANAGER_TEST
enum MemoryChunkCategory
{
	  MC_DEFAULT = 0
	, MC_SYSTEM
	, MC_END			// Required to enable counting of all categories.
};

struct MemoryChunk
{
	AEByte*			data;				//!< Chunk memory data.
	std::size_t		chunkTotalSize;		//!< Total chunk memory size.
	std::size_t		usedSize;			//!< Used up memory amount.
};

size_t MemoryCategories[] = {
	  MC_DEFAULT
	, MC_SYSTEM
};

size_t MemoryCategorySizes[] = {
	  8388608	// 8Mb
	, 8388608	// 8Mb
};

class MemoryManager
{
	public:
		MemoryManager()
		: mTotalPoolSize(0)
		, mUsedMemorySize(0)
		, mFreeMemorySize(0)
		, mMemoryPool(0)
		{
			for (AEUint categoryIndex(0U); categoryIndex < MC_END; ++categoryIndex)
				mTotalPoolSize += MemoryCategorySizes[categoryIndex];

			AELOG(LOG_INFO) << "Memory Pool created at total size of: " << mTotalPoolSize << "Bytes";
			mFreeMemorySize = mTotalPoolSize;

			mMemoryPool = reinterpret_cast<AEByte*>(malloc(mTotalPoolSize));

			for (AEUint categoryIndex(0U); categoryIndex < MC_END; ++categoryIndex)
			{
				size_t categorySize(MemoryCategorySizes[categoryIndex]);
				size_t targetSize(MemoryCategorySizes[categoryIndex - 1]);
				MemoryChunk& currentChunk(mMemoryChunks[categoryIndex]);

				if (categoryIndex == 0U)
					targetSize = 0;

				currentChunk.data			= mMemoryPool + (sizeof(AEByte) * targetSize);
				currentChunk.chunkTotalSize = categorySize;
				currentChunk.usedSize		= 0;
				memset(currentChunk.data , categoryIndex + 1, currentChunk.chunkTotalSize);	// Initialise the whole memory to the category value.
			}
		}

		MemoryManager(const MemoryManager&)
		: mTotalPoolSize(0)
		, mUsedMemorySize(0)
		, mFreeMemorySize(0)
		, mMemoryPool(0)
		{
		}

		MemoryManager& operator=(const MemoryManager&)
		{
			return *this;
		}

		~MemoryManager()
		{
			delete mMemoryPool;
		}

		void* allocate(size_t size, const MemoryChunkCategory category)
		{
			MemoryChunk& activeChunk(mMemoryChunks[category]);
			// We do not know the allocation size memory is finally free. This info is added into the 4bytes padded at the beginning of the allocation.
			AEUint* sizeLocation(reinterpret_cast<AEUint*>(activeChunk.data + (sizeof(AEByte) * activeChunk.usedSize)));
			*sizeLocation = size;
			activeChunk.usedSize += sizeof(AEUint); // Move to the beginning of the actual data address.

			void* location(reinterpret_cast<void*>(activeChunk.data + (sizeof(AEByte) * activeChunk.usedSize)));
			activeChunk.usedSize += size;
			memset(location, '-', size);

			AELOG(LOG_INFO) << "Memory allocated to chunk category: " << static_cast<AEUint>(category) << " Size: " << (size + 4) << " Used Size: " << activeChunk.usedSize << " Available: " << (activeChunk.chunkTotalSize - activeChunk.usedSize);

			return location;
		}

		void deallocate(void* address)
		{
			// Check which category this address is at:
			AEUint currentCategory(INVALID);
			for (AEUint categoryIndex(0U); categoryIndex < MC_END; ++categoryIndex)
			{
				const MemoryChunk& chunk(mMemoryChunks[categoryIndex]);
				if ((address >= chunk.data) && (address < (chunk.data + (sizeof(AEByte) * chunk.chunkTotalSize))))
					currentCategory = categoryIndex;
			}

			AEByte* fullData(reinterpret_cast<AEByte*>(address));
			fullData -= sizeof(AEUint);	// Include the 4 bytes appended that store the allocation size.
			AEUint& size(*reinterpret_cast<AEUint*>(fullData));
			size += sizeof(AEUint);

			MemoryChunk& activeChunk(mMemoryChunks[currentCategory]);
			activeChunk.usedSize -= size;

			AELOG(LOG_INFO) << "Memory freed from chunk category: " << currentCategory << " Size: " << size << " Used Size: " << activeChunk.usedSize << " Available: " << (activeChunk.chunkTotalSize - activeChunk.usedSize);
			memset(fullData, '/', size);
		}

	private:
		AEInt 			mTotalPoolSize;
		AEInt 			mUsedMemorySize;
		AEInt 			mFreeMemorySize;
		MemoryChunk		mMemoryChunks[MC_END];
		AEByte*			mMemoryPool;
};

struct TestStruct
{
	AEByte test1;
	AEUint test2;
};

#endif

int main(int argc, char* argv[])
{
#ifdef CUSTOM_MEMORY_MANAGER_TEST
	MemoryManager testMemoryManager;

	TestStruct* newStruct(reinterpret_cast<TestStruct*>(testMemoryManager.allocate(sizeof(TestStruct), MC_DEFAULT)));
	TestStruct* newStruct3(reinterpret_cast<TestStruct*>(testMemoryManager.allocate(sizeof(TestStruct), MC_DEFAULT)));
	TestStruct* newStruct2(reinterpret_cast<TestStruct*>(testMemoryManager.allocate(sizeof(TestStruct), MC_SYSTEM)));
	TestStruct* newStruct4(reinterpret_cast<TestStruct*>(testMemoryManager.allocate(sizeof(TestStruct), MC_SYSTEM)));

	testMemoryManager.deallocate(newStruct);
	testMemoryManager.deallocate(newStruct3);
	testMemoryManager.deallocate(newStruct2);
	testMemoryManager.deallocate(newStruct4);
#endif

	glutInit(&argc, argv); // Used only for refering to rendering predefined models. Will be removed soon enough.

	AERenderSystem& renderSystem(AERenderSystem::getInstance());
	renderSystem.createWindow(TITLE, 800U, 600U, false);
	renderSystem.setCurrentContext(renderSystem.createRenderContext());
	renderSystem.initialiseRenderer();
	renderSystem.enableRenderParameter(RENDER_PARAM_DEPTH_TEST |
									   RENDER_PARAM_BACK_FACE_CULLING |
									   RENDER_PARAM_LINE_SMOOTHING |
									   RENDER_PARAM_LIGHTING
									  );
	renderSystem.enableLight(0);
	renderSystem.enableLight(1);
	renderSystem.enableLight(2);
	renderSystem.setClearColor(AEColor(0.35F, 0.35F, 0.35F, 1.0F));
	{
#if 0
		AEVector3 testVector3;
		AEVector4 testVector4;

		// Section unit test for FixedArray class.
		AEFixedArray<AEUint> testArray(10U);
		testArray.pushBack(3U);
		testArray.pushBack(4U);
		testArray.pushBack(5U);

		testArray.at(2U) = 5U;

		testArray.popBack();

		AEFixedArray<AEUint> copyArray(testArray);

		AEFixedArray<TestClass> testClassArray(5U);
		testClassArray.pushBack(TestClass());
		testClassArray.pushBack(TestClass());
		testClassArray.pushBack(TestClass());

		AEFixedArray<TestClass> newTestClassArray;

		newTestClassArray = testClassArray;

		TestClass* startTestClass(testClassArray);
		printf("Added at startTestClass 0x%02x\n", *reinterpret_cast<AEUint*>(startTestClass));
#endif
	}

	bool runApp(true);

	AEFloat32 rotation(0.0F);

	testTexture = SOIL_load_OGL_texture("test_image.tga", SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);
	AEMaterial defaultMaterial("DefaultMaterial", AEColor(0.2F, 0.2F, 0.2F, 1.0F), AEColor(1.0F, 0.0F, 1.0F, 0.5F), 0.5F, 128.0F);

#ifdef CUSTOM_MEMORY_MANAGER_TEST
	AEBitMapFont* fontPrinter(reinterpret_cast<AEBitMapFont*>(testMemoryManager.allocate(sizeof(AEBitMapFont), MC_DEFAULT)));
#else
	AEBitMapFont* fontPrinter(new AEBitMapFont());
#endif
	fontPrinter->initialise();

	AE::LOG_LEVEL = LOG_ALL;	// Log everything and up to including errors.

//	{	// Tests log infos
//		AEFixedArray<AEUint> testArray;
//		testArray.pushBack(1U);
//		testArray.pushBack(2U);
//		testArray.at(2);
//	}

	// Quick test of planes class.
	AEPlane newPlane;
	newPlane.generateFromPoints(AEVector3(20.0F, 0.0F, 0.0F), AEVector3(0.0F, 20.0F, 0.0F), AEVector3(0.0F, 0.0F, 40.0F));

	AELOG(LOG_INFO) << "Plane distance from origin is: " << newPlane.getDistance();
	AELOG(LOG_INFO) << "Point intersect status: " << static_cast<AEInt32>(newPlane.checkPointIntersect(AEVector3(0.0F, 20.0F, 0.0F)));

	{ // Memory pool tests:
	}

#ifdef SHADER_TEST
	//AEFixedArray<GLuint> shaderList(2U);
	// Create shaders Steps:
	// First create the vertex shader:
	// No filesystem yet in the engine. It's on the lists of things to add. Char buffer will do for the time being:
	GLuint vertexShader(glCreateShader(GL_VERTEX_SHADER));
	AEString defaultVertexShader =
		//"layout(location = 0) in vec4 position;" "\n"
		"attribute vec4 a_position;"		"\n"
		"attribute vec4 a_color;"		"\n"
		"uniform mat4 u_worldMatrix;"		"\n"
		"varying vec4 colour;"			"\n"
		"void main()"					"\n"
		"{"								"\n"
		//"	gl_Position = position;"	"\n"
		"	vec4 newPosition = u_worldMatrix * a_position;"	"\n"
		"	newPosition = gl_ModelViewMatrix * newPosition;"	"\n"	// The gl_ModelViewMatrix is essentiall Viewmatrix * Identity Matrix
		//"	gl_Position = u_worldMatrix * a_position;"	"\n"
		"	gl_Position = gl_ProjectionMatrix * newPosition;"	"\n"
		"	colour = a_color;"	"\n"
		"}"								"\n";
	const char* charPath = defaultVertexShader.c_str();
	// Bind the file to the vertex shader.
	glShaderSource(vertexShader, 1, &charPath, NULL);
	// Compile the shader:
	glCompileShader(vertexShader);
	// Check on the status of the compilation:
	GLint status;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		// Log out some extra info about the compiled shader:
		GLint infoLogLength;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* stringInfoLog(new GLchar[infoLogLength + 1]);
		glGetShaderInfoLog(vertexShader, infoLogLength, NULL, stringInfoLog);
		
		const AEString shaderLog(stringInfoLog);
		delete[] stringInfoLog;

		AELOG(LOG_INFO) << shaderLog;
	}
	else
		AELOG(LOG_INFO) << "Vertex shader compilation successful.";

	// Fragment shader creation:
	const GLuint fragmentShader(glCreateShader(GL_FRAGMENT_SHADER));
	AEString defaultFragmentShader =
		"varying vec4 colour;"								"\n"
		"void main()"										"\n"
		"{"													"\n"
		//"	float lerpValue = gl_FragCoord.y / 500.0F;"		"\n"
		//"	gl_FragColor = mix(vec4(1.0F, 1.0F, 1.0F, 1.0F), vec4(0.2F, 0.2F, 0.2F, 1.0F), lerpValue);"	"\n"
		"	gl_FragColor = colour;"			"\n"
		"}"													"\n";
	charPath = defaultFragmentShader.c_str();
	// Bind fragment shader to the string buffer:
	glShaderSource(fragmentShader, 1, &charPath, NULL);
	// Compile fragment shader:
	glCompileShader(fragmentShader);
	// Get the status of the compilation:
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
	
	if (status == GL_FALSE)
	{
		// Log out some extra info about the compiled shader:
		GLint infoLogLength;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* stringInfoLog(new GLchar[infoLogLength + 1]);
		glGetShaderInfoLog(fragmentShader, infoLogLength, NULL, stringInfoLog);
		
		const AEString shaderLog(stringInfoLog);
		delete[] stringInfoLog;

		AELOG(LOG_INFO) << shaderLog;
	}
	else
		AELOG(LOG_INFO) << "Fragment shader compilation successful.";

	// Create the shader program that attaches both these shaders together:
	const GLuint defaultShaderProgram(glCreateProgram());
	// Attach the vertex and fragment shader to this program:
	glAttachShader(defaultShaderProgram, vertexShader);
	glAttachShader(defaultShaderProgram, fragmentShader);
	// Bind attributes to the program.
	glBindAttribLocation(defaultShaderProgram, 0, "a_position");
	glBindAttribLocation(defaultShaderProgram, 1, "a_color");
	// Link them inside the program:
	glLinkProgram(defaultShaderProgram);
	// Check the status of the program linking:
	GLint linkStatus;
	glGetProgramiv(defaultShaderProgram, GL_LINK_STATUS, &linkStatus);

	if (linkStatus == GL_FALSE)
	{
		GLint infoLogLength;
		glGetProgramiv(defaultShaderProgram, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar* stringInfoLog(new GLchar[infoLogLength + 1]);
		glGetProgramInfoLog(defaultShaderProgram, infoLogLength, NULL, stringInfoLog);

		const AEString shaderLog(stringInfoLog);
		delete[] stringInfoLog;

		AELOG(LOG_INFO) << shaderLog;
	}
	else
		AELOG(LOG_INFO) << "Shader programe link successful.";

	// We can freely detach these shader objects from the program as they are now already successfully linked:
	glDetachShader(defaultShaderProgram, vertexShader);
	glDetachShader(defaultShaderProgram, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	//const float vertexPositions[] =
	//{
	//	0.0F, 0.0F, 0.0F, 1.0F,
	//	1.0F, 0.0F, 0.0F, 1.0F,
	//	0.0F, 1.0F, 0.0F, 1.0F,
	//};

//	// Much more complicated vertex buffer data - containing colour for each vertices. Data is not interleaved. Order: Position -> Colour
//	const float vertexData[] =
//	{
//		// Positions:
//		0.0F, 0.0F, 0.0F, 1.0F,
//		1.0F, 0.0F, 0.0F, 1.0F,
//		0.0F, 1.0F, 0.0F, 1.0F,
//		// Colours:
//		1.0F, 0.0F, 0.0F, 1.0F,
//		1.0F, 1.0F, 0.0F, 1.0F,
//		0.0F, 1.0F, 1.0F, 1.0F,
//	};

	// Much more complicated vertex buffer data - containing colour for each vertices. Data is interleaved. Order: Position -> Colour per vertex
	// Can also be much faster.
	const float vertexData[] =
	{
		0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F, 1.0F, // Vertex 1: Position(4)-Colour(4)
		1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, 1.0F, // Vertex 2: Position(4)-Colour(4)
		0.0F, 1.0F, 0.0F, 1.0F, 0.0F, 1.0F, 1.0F, 1.0F, // Vertex 3: Position(4)-Colour(4)
	};

	// Steps to binding buffers to OGL
	GLuint positionBuffer;
	glGenBuffers(1, &positionBuffer);	// Generate buffer link.
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);	// Bind that buffer to the created link.
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Shader uniforms setups:
	GLint worldMatrix = glGetUniformLocation(defaultShaderProgram, "u_worldMatrix");
	if (worldMatrix == -1)
	{
		AELOG(LOG_INFO) << "u_worldMatrix not found in shader.";
	}

	AEVector3 trianglePosition(0.0F, 0.0F, 2.0F);
	AEMatrix4x4 triangleTransform;
	triangleTransform.setToIdentity();
	triangleTransform.setColumn(3U, AEVector4(trianglePosition));

	// This will require transposing when passed to the shader.
	//GLfloat transform[] =
	//{
	//	1.0F, 0.0F, 0.0F, trianglePosition.x(),
	//	0.0F, 1.0F, 0.0F, trianglePosition.y(),
	//	0.0F, 0.0F, 1.0F, trianglePosition.z(),
	//	0.0F, 0.0F, 0.0F, 1.0F,
	//};

#endif
	while (runApp)
	{
		runApp = !AERenderSystem::getInstance().handleWindowEvent(); // handleWindowEvent return true if closing is called. This should be managed outside instead. (use events or messages).

		if (rotation >= 360.0F)
			rotation = 0.0F;

		rotation += 0.01F;

		if (testTexture == 0)
		{
			AELOG(LOG_INFO) << "SOIL texture load error: " << SOIL_last_result();
		}

		AERenderSystem::getInstance().startFrame();
		{

#if defined(SHADER_TEST)
			glUseProgram(defaultShaderProgram);
			glUniformMatrix4fv(worldMatrix, 1, GL_FALSE, triangleTransform);
			//glUniformMatrix4fv(worldMatrix, 1, GL_TRUE, transform);

  			gluLookAt(0.0F, 5.0F, 7.0F, 0, 0, 0, 0, 1, 0);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);

//			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
//			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*)48);	// For the non-interleaved data.

			// Each piece of vertex data contains 32 bytes of info -> stride size.
			// Position is from offset 0 within the stride.
			// Colour is 16 bytes after the position. 0 + (4*4)
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 32, 0);
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 32, (GLvoid*)16);	// For the interleaved data.

			glDrawArrays(GL_TRIANGLES, 0, 3);

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glUseProgram(0);

			renderSystem.disableRenderParameter(RENDER_PARAM_TEXTURING);
			renderSystem.enableRenderParameter(RENDER_PARAM_WIRE_FRAME);
			drawGrid(6U, 1.0F);
			drawGizmo(0.0F, 0.01F, 0.0F);
			renderSystem.enableRenderParameter(RENDER_PARAM_TEXTURING);
			renderSystem.disableRenderParameter(RENDER_PARAM_WIRE_FRAME);


#elif defined(FIXED_PIPELINE)
			gluLookAt(0.0F, 5.0F, 7.0F, 0, 0, 0, 0, 1, 0);
			//glRotatef(rotation, 0.0F, 1.0F, 0.0F); // Rotate view around scene center.

			// Lighting
			// Simulate close to realistic lighting application
			// Were the bottom parts of objects are slightly darker than the top parts.
			// Then add a directional light as sun representation.
			glPushMatrix();
				// This is a directional light pointing directly downwards and lighter than our global default ambient light.
				GLfloat lightColor[] = { 0.2F, 0.2F, 0.3F, 1.0F };
				glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
				GLfloat lightColor2[] = { 0.1F, 0.1F, 0.1F, 1.0F }; 
				glLightfv(GL_LIGHT0, GL_AMBIENT, lightColor2);
				GLfloat lightColor3[] = { 1.0F, 1.0F, 1.0F, 1.0F };
				glLightfv(GL_LIGHT0, GL_SPECULAR, lightColor3);

				GLfloat lightDirection[] = { 0.0F, 1.0F, 0.0F, 0.0F };
				glLightfv(GL_LIGHT0, GL_POSITION, lightDirection);
			glPopMatrix();

			glPushMatrix();
				// This is our sun light representation.
				glRotatef(rotation * 3.0F, 1.0F, 1.0F, 0.0F); 
				GLfloat lightColor1[] = { 0.8F, 0.8F, 0.7F, 1.0F };
				glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor1);
				glLightfv(GL_LIGHT1, GL_SPECULAR, lightColor3);
				GLfloat lightDirection1[] = { 0.0F, 0.0F, 1.0F, 0.0F };
				glLightfv(GL_LIGHT1, GL_POSITION, lightDirection1);
			glPopMatrix();

			glPushMatrix();
				// This is our sun light representation.
				glRotatef(rotation * 3.0F, 1.0F, 1.0F, 0.0F); 
				GLfloat lightColor4[] = { 0.5F, 0.7F, 0.1F, 1.0F };
				glLightfv(GL_LIGHT2, GL_DIFFUSE, lightColor4);
				glLightfv(GL_LIGHT2, GL_SPECULAR, lightColor3);
				GLfloat lightDirection2[] = { 0.0F, 1.0F, -1.0F, 0.0F };
				glLightfv(GL_LIGHT2, GL_POSITION, lightDirection2);
			glPopMatrix();

			glPushMatrix();
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, defaultMaterial.getAmbientColor());
					glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultMaterial.getDiffuseColor());
					glMaterialfv(GL_FRONT, GL_SPECULAR, defaultMaterial.getSpecularColor());
					glMaterialf(GL_FRONT, GL_SHININESS, defaultMaterial.getShininess()); 
					glMaterialfv(GL_FRONT, GL_EMISSION, defaultMaterial.getEmissiveColor());
				}
				glTranslatef(2.0F, 0.5F, 0.0F);
				glRotatef(rotation, 1.0F, 0.0F, 0.0F);
				glutSolidTorus(0.2F, 0.5F, 50.0F, 50.0F);

				glPushMatrix();
					{
						glMaterialfv(GL_FRONT, GL_AMBIENT, defaultMaterial.getAmbientColor());
						glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultMaterial.getDiffuseColor());
						glMaterialfv(GL_FRONT, GL_SPECULAR, defaultMaterial.getSpecularColor());
						glMaterialf(GL_FRONT, GL_SHININESS, defaultMaterial.getShininess()); 
						glMaterialfv(GL_FRONT, GL_EMISSION, defaultMaterial.getEmissiveColor());
					}

					glTranslatef(1.0F, 0.5F, 0.0F);
					glutSolidTorus(0.2F, 0.5F, 50.0F, 50.0F);
				glPopMatrix();
			glPopMatrix();

			renderSystem.disableRenderParameter(RENDER_PARAM_DEPTH_TEST);
			renderSystem.enableRenderParameter(RENDER_PARAM_TEXTURING | RENDER_PARAM_ALPHA_BLEND);	
			glPushMatrix();
				glTranslatef(-2.0F, 0.0F, 0.0F);
				{
					AEMaterial goldMaterial("GoldMaterial", AEColor(0.9F, 0.8F, 0.2F, 1.0F), AEColor(1.0F, 1.0F, 1.0F, 1.0F), 0.5F, 128.0F);
					glMaterialfv(GL_FRONT, GL_AMBIENT, goldMaterial.getAmbientColor());
					glMaterialfv(GL_FRONT, GL_DIFFUSE, goldMaterial.getDiffuseColor());
					glMaterialfv(GL_FRONT, GL_SPECULAR, goldMaterial.getSpecularColor());
					glMaterialf(GL_FRONT, GL_SHININESS, goldMaterial.getShininess()); 
					glMaterialfv(GL_FRONT, GL_EMISSION, goldMaterial.getEmissiveColor());
				}
				drawCube(1.0F, 1.0F, 0.0F, 0.0F);
			glPopMatrix();

			glPushMatrix();
				glTranslatef(-1.5F, 0.0F, 0.5F);
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, defaultMaterial.getAmbientColor());
					glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultMaterial.getDiffuseColor());
					glMaterialfv(GL_FRONT, GL_SPECULAR, defaultMaterial.getSpecularColor());
					glMaterialf(GL_FRONT, GL_SHININESS, defaultMaterial.getShininess()); 
					glMaterialfv(GL_FRONT, GL_EMISSION, defaultMaterial.getEmissiveColor());
				}
				drawCube(1.0F, 1.0F, 1.0F, 0.0F);
			glPopMatrix();
			renderSystem.disableRenderParameter(RENDER_PARAM_TEXTURING | RENDER_PARAM_ALPHA_BLEND);
			renderSystem.enableRenderParameter(RENDER_PARAM_DEPTH_TEST);

			glPushMatrix();
				{
					glMaterialfv(GL_FRONT, GL_AMBIENT, defaultMaterial.getAmbientColor());
					glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultMaterial.getDiffuseColor());
					glMaterialfv(GL_FRONT, GL_SPECULAR, defaultMaterial.getSpecularColor());
					glMaterialf(GL_FRONT, GL_SHININESS, defaultMaterial.getShininess()); 
					glMaterialfv(GL_FRONT, GL_EMISSION, defaultMaterial.getEmissiveColor());
				}
				glTranslatef(0.5F, 0.5F, -2.0F);
				glutSolidSphere(1.0F, 80.0F, 80.0F);
			glPopMatrix();

			//glPushMatrix();
			//	{
			//		glMaterialfv(GL_FRONT, GL_AMBIENT, defaultMaterial.getAmbientColor());
			//		glMaterialfv(GL_FRONT, GL_DIFFUSE, defaultMaterial.getDiffuseColor());
			//		glMaterialfv(GL_FRONT, GL_SPECULAR, defaultMaterial.getSpecularColor());
			//		glMaterialf(GL_FRONT, GL_SHININESS, defaultMaterial.getShininess()); 
			//		glMaterialfv(GL_FRONT, GL_EMISSION, defaultMaterial.getEmissiveColor());
			//	}
			//	glTranslatef(2.0F, 0.0F, 0.0F);
			//	glutSolidCube(1.0F);
			//glPopMatrix();

			//AERenderSystem::getInstance().enableWireFrameMode();
			//glPushMatrix();
			//	glColor4f(0.9F, 0.0F, 0.7F, 1.0F);
			//	glTranslatef(2.0F, 0.0F, 2.0F);
			//	// Teapot vertex winding on glut teapots are clockwise, our back face culling is clockwise
			//	// therefore just temporarily tell GL that the front face winding is clockwise mode, then put it back to counter-clockwise.
			//	glFrontFace(GL_CW);
			//	glutSolidTeapot(1.0F);
			//	glFrontFace(GL_CCW);
			//glPopMatrix();
			//AERenderSystem::getInstance().disableWireFrameMode();

			// Material affected poly
			glPushMatrix();
				{
					AEMaterial goldMaterial("GoldMaterial", COLOR_BLACK, AEColor(0.9F, 0.8F, 0.2F, 1.0F), 0.5F, 128.0F);

					glMaterialfv(GL_FRONT, GL_AMBIENT, goldMaterial.getAmbientColor());
					glMaterialfv(GL_FRONT, GL_DIFFUSE, goldMaterial.getDiffuseColor());
					glMaterialfv(GL_FRONT, GL_SPECULAR, goldMaterial.getSpecularColor());
					glMaterialf(GL_FRONT, GL_SHININESS, goldMaterial.getShininess()); 
					glMaterialfv(GL_FRONT, GL_EMISSION, goldMaterial.getEmissiveColor());
				}

				// Ambient and Diffuse are colors
				// emissivePower is a float ranging 0.0F to 1.0F. Color is the diffuse color times emissive power.
				// Add Transparency setting in material and so we can check for transparency if needed.
				// Use transparency value to ovewrite last color param in diffuse color.
				// Specularity is how much a light specular is shone onto the surface material. Call it specularBrightness.
				// Specular color is not set by the material but by the light.
				glTranslatef(-1.0F, 0.0F, 2.0F);
				glutSolidSphere(1.0F, 100.0F, 100.0F);
			glPopMatrix();

			renderSystem.disableRenderParameter(RENDER_PARAM_TEXTURING);
			renderSystem.enableRenderParameter(RENDER_PARAM_WIRE_FRAME);
			drawGrid(6U, 1.0F);
			drawGizmo(1.0F, 0.01F, 0.0F);
			renderSystem.enableRenderParameter(RENDER_PARAM_TEXTURING);
			renderSystem.disableRenderParameter(RENDER_PARAM_WIRE_FRAME);

			// Go to 2D orthogonal mode.
			//renderSystem.disableRenderParameter(RENDER_PARAM_LIGHTING);
			//fontPrinter->fontPrint("Hello World Text on screen.");
			//renderSystem.enableRenderParameter(RENDER_PARAM_LIGHTING);

#endif



		}
		renderSystem.endFrame();
	}

	//delete triangleTransform;
#ifdef CUSTOM_MEMORY_MANAGER_TEST
	testMemoryManager.deallocate(fontPrinter);
#else
	delete fontPrinter;
#endif

#ifdef SHADER_TEST
	glDeleteProgram(defaultShaderProgram);
#endif

	renderSystem.destroyWindow();

	AELOG(LOG_INFO) << "Application terminated!";

	return 0;
}
#endif

#if defined (WIN32)
#include <windows.h>

#include "AESystemTypes.h"
#include "AELogger.h"
#include "AERenderSystem.h"
#include "AESingleton.h"

using namespace AE;

void drawScene();
void drawGrid(const AEUint32 gridSquareSize, const AEFloat32 gridSpace);

void drawScene()
{
	gluLookAt(0, 0, 6, 0, 0, -6, 0, 1, 0);
	glTranslatef(-1.5F, 0.0F, -6.0F);

	glBegin(GL_TRIANGLES);
		glColor4f(1.0F, 0.0F, 0.0F, 1.0F);
		glVertex3f(0.0F, 1.0F, 0.0F);		// Top point.
		glColor4f(0.0F, 1.0F, 0.0F, 1.0F);
		glVertex3f(-1.0F, -1.0F, 0.0F);		// Bottom left point.
		glColor4f(0.0F, 0.0F, 1.0F, 1.0F);
		glVertex3f(1.0F, -1.0F, 0.0F);		// Bottom right point.
	glEnd();

	glColor4f(0.5F, 0.0F, 0.05F, 1.0F);
	glTranslatef(3.0F, 0.0F, 0.0F);
	glBegin(GL_QUADS);
		glVertex3f(-1.0F, 1.0F, 0.0F);		// Top left point.
		glVertex3f(1.0F, 1.0F, 0.0F);		// Top right point.
		glVertex3f(1.0F, -1.0F, 0.0F);		// Bottom right point.
		glVertex3f(-1.0F, -1.0F, 0.0F);		// Bottom left point.
	glEnd();
}

void drawGrid(const AEUint32 gridSquareSize, const AEFloat32 gridSpace)
{
	const AEFloat32 halfLineCount(gridSquareSize * 0.5F);

	glDisable(GL_LIGHTING);
	glColor4f(0.1F, 0.1F, 0.1F, 1.0F);
	glPushMatrix();
		for (AEFloat32 lineIndex(-halfLineCount); lineIndex <= halfLineCount; lineIndex += gridSpace)
		{
			glBegin(GL_LINES);
				// Horizontal line
				glVertex3f(static_cast<AEFloat32>(-halfLineCount), 0.0F, static_cast<AEFloat32>(lineIndex));
				glVertex3f(static_cast<AEFloat32>(halfLineCount), 0.0F, static_cast<AEFloat32>(lineIndex));
				// Vertical line
				glVertex3f(static_cast<AEFloat32>(lineIndex), 0.0F, static_cast<AEFloat32>(-halfLineCount));
				glVertex3f(static_cast<AEFloat32>(lineIndex), 0.0F, static_cast<AEFloat32>(halfLineCount));
			glEnd();
		}
	glPopMatrix();
	glEnable(GL_LIGHTING);
}

//Test singleton class
class TestSingleton : public Singleton<TestSingleton>
{
	friend class Singleton<TestSingleton>;

	private:
		TestSingleton() { AELOG(LOG_INFO) << "Instance of test class created"; }
};

bool runApp(true);

LRESULT CALLBACK WndProc(HWND hwnd, unsigned int msg, WPARAM wParam,LPARAM lParam)
{
	switch(msg)
	{
		case WM_DESTROY:
		case WM_CLOSE:													// Closing The Window
			AELOG(LOG_INFO) << "Quit Message. From callback." << AERenderSystem::getInstance().getWindowHandle();
			PostMessage(AERenderSystem::getInstance().getWindowHandle(), WM_QUIT, 0, 0);							// Send A WM_QUIT Message
			runApp = false;
			break;
		case WM_SIZE:													// Size Action Has Taken Place
			switch (wParam)												// Evaluate Size Action
			{
				case SIZE_MINIMIZED:									// Was Window Minimized?
					return 0;												// Return
				case SIZE_MAXIMIZED:									// Was Window Maximized?
					AERenderSystem::getInstance().resizeRenderSystem(LOWORD(lParam), HIWORD (lParam));		// Reshape Window - LoWord=Width, HiWord=Height
					return 0;												// Return
				case SIZE_RESTORED:										// Was Window Restored?
					AERenderSystem::getInstance().resizeRenderSystem(LOWORD (lParam), HIWORD (lParam));		// Reshape Window - LoWord=Width, HiWord=Height
				return 0;												// Return
			}
		break;															// Break
	}
	return (DefWindowProc(hwnd, msg, wParam, lParam));
}


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE, LPSTR, int)
//int main()
{
	AELOG_INIT;
	AE::LOG_LEVEL = LOG_ALL;	// Log everything and up to including errors.
	const AEString TITLE("Affinity Engine OpenGL Render System");
	AERenderSystem& renderSystem(AERenderSystem::getInstance());
	renderSystem.setInstanceHandle(hInstance);
	renderSystem.setCallbackProcedure(WndProc);
	renderSystem.createWindow(TITLE, 800U, 600U, false);
	renderSystem.setCurrentContext(renderSystem.createRenderContext());
	renderSystem.initialiseRenderer();
	renderSystem.enableRenderParameter(RENDER_PARAM_DEPTH_TEST |
									   RENDER_PARAM_BACK_FACE_CULLING |
									   RENDER_PARAM_LINE_SMOOTHING |
									   RENDER_PARAM_LIGHTING
									  );

	renderSystem.enableLight(0);
	renderSystem.enableLight(1);
	renderSystem.enableLight(2);
	renderSystem.setClearColor(AEColor(0.35F, 0.35F, 0.35F, 1.0F));
	MSG message;
	while (runApp)
	{

		if (PeekMessage (&message, renderSystem.getWindowHandle(), 0, 0, PM_REMOVE) != 0)
		{
			// Check For WM_QUIT Message
			if (message.message != WM_QUIT)						// Is The Message A WM_QUIT Message?
			{
				DispatchMessage(&message);						// If Not, Dispatch The Message
			}
			else											// Otherwise (If Message Is WM_QUIT)
			{
				AELOG(LOG_INFO) << "Quit Message.";
				runApp = false;
			}
		}
		else
		{
			AERenderSystem::getInstance().startFrame();
				gluLookAt(0.0F, 5.0F, 7.0F, 0, 0, 0, 0, 1, 0);
					glPushMatrix();
						drawGrid(6U, 0.5F);
						drawScene();
					glPopMatrix();
			AERenderSystem::getInstance().endFrame();
		}
	}

	AELOG(LOG_INFO) << "Destroying windows.";
	renderSystem.destroyWindow();
	AELOG_DESTROY;
	return 0;
}
#endif

