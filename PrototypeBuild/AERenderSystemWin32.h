#ifndef AE_RENDER_SYSTEM_WIN32_H_
#define AE_RENDER_SYSTEM_WIN32_H_

#include <windows.h>
#include <vector>
#include <GL/gl.h>
#include <GL/glu.h>
#include "AERenderSystemBase.h"

namespace AE
{

/*!\brief Handles all win32 rendering controls
 *
 * Full control over the OpenGL rendering.
 */
class AERenderSystem : public AERenderSystemBase
{
	public:
		static AERenderSystem& getInstance();

		void createWindow(const AEString& title, const AEUint screenWidth, const AEUint screenHeight, const bool fullscreen);
		void destroyWindow();
		AEContextHandle createRenderContext();
		void setCurrentContext(const AEContextHandle contextHandle);

		//! Function initialise graphic render API (OpenGL for linux)
		void initialiseRenderer();

		void setClearColor(const AEColor& color);

		void enableRenderParameter(const AEUint params);
		void disableRenderParameter(const AEUint params);

		void enableLight(const AEInt light);
		void disableLight(const AEInt light);

		void startFrame();
		void endFrame();
		void resizeRenderSystem(AEInt width, AEInt height);

		void setInstanceHandle(const HINSTANCE instanceHandle) { mHandleInstance = instanceHandle; }
		void setCallbackProcedure(WNDPROC procedure) { mCallBackProcedure = procedure; }

		const HWND getWindowHandle() const { return mHandleWindow; }

	private:
		AERenderSystem();
		AERenderSystem(const AERenderSystem&);
		const AERenderSystem& operator=(const AERenderSystem&) { return *this; }
		//AERenderSystem& operator=(AERenderSystem&) { return *this; }

		void enableDepthTest();
		void enableBackFaceCulling();
		void enableAlphaBlend();
		void enableLineSmoothing();
		void enableWireFrameMode();
		void enableLighting();
		void enableTexture();

		void disableDepthTest();
		void disableBackFaceCulling();
		void disableAlphaBlend();
		void disableLineSmoothing();
		void disableWireFrameMode();
		void disableLighting();
		void disableTexture();

		std::vector<HGLRC>		mContextList;

		static AERenderSystem*	mRenderSystem;

		HINSTANCE				mHandleInstance;
		HWND					mHandleWindow;
		HDC						mHandleDeviceContext;						// Device Context
		WNDPROC					mCallBackProcedure;

		AEInt					mScreenWidth;
		AEInt					mScreenHeight;
		bool					mFullScreen;
};

}	// namespace AE

#endif // AE_RENDER_SYSTEM_WIN32_H_

