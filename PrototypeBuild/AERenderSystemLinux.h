#ifndef AE_RENDER_SYSETM_LINUX_H_
#define AE_RENDER_SYSETM_LINUX_H_

// Linux using X windows specific includes.
#include <GL/glew.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <X11/extensions/xf86vmode.h>	// Requires libxxf86vm-dev installed.
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <vector>
// Temp
#include <GL/glut.h>

#include "AERenderSystemBase.h"

namespace AE {

/*! \brief Handles all linux rendering controls
 *
 * Full control over the OpenGL rendering.
 */
class AERenderSystem : public AERenderSystemBase
{
	public:
		static AERenderSystem& getInstance();

		void createWindow(const AEString& title, const AEUint screenWidth, const AEUint screenHeight, const bool fullscreen);
		void destroyWindow();
		AEContextHandle createRenderContext();
		void setCurrentContext(const AEContextHandle contextHandle);

		//! Function initialise graphic render API (OpenGL for linux)
		void initialiseRenderer();

		void setClearColor(const AEColor& color);

		void enableRenderParameter(const AEUint params);
		void disableRenderParameter(const AEUint params);

		void enableLight(const AEInt light);
		void disableLight(const AEInt light);

		void startFrame();
		void endFrame();

		/*!\brief Resize render screen.
		 * 
		 * Using the width and height parameter the render screen context resolution is changed to
		 * the selected values.
		 * \param width Target width resolution.
		 * \param height Target height resolution.
		 * \return void No return.
		 * \sa startFrame()
		 */
		void resizeRenderSystem(AEInt width, AEInt height);
		
		//! Handles only event inputs occurring for the window.
		//! Return false only when window is called for termination.
		//! Temporary function. As we don't have an input subsystem yet.
		bool handleWindowEvent();

	private:
		AERenderSystem();
		AERenderSystem(const AERenderSystem&);
		const AERenderSystem& operator=(const AERenderSystem&) { return *this; }
		AERenderSystem& operator=(AERenderSystem&) { return *this; }

		void enableDepthTest();
		void enableBackFaceCulling();
		void enableAlphaBlend();
		void enableLineSmoothing();
		void enableWireFrameMode();
		void enableLighting();
		void enableTexture();

		void disableDepthTest();
		void disableBackFaceCulling();
		void disableAlphaBlend();
		void disableLineSmoothing();
		void disableWireFrameMode();
		void disableLighting();
		void disableTexture();

		std::vector<GLXContext>			mContextList;

		static AERenderSystem*			mRenderSystem;

		Display*						mDisplay;
		XVisualInfo*					mVisualInfo;
		XF86VidModeModeInfo				mVideoMode;
		Window							mWindow;

		AEInt							mScreenHandle;
		AEInt							mScreenWidth;
		AEInt							mScreenHeight;
		bool							mFullScreen;

		bool							mGlewInitialised;
};

} // namespace AE

#endif // AE_RENDER_SYSETM_LINUX_H_

