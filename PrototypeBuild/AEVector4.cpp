#include "AEVector4.h"

#include "AEVector3.h"

namespace AE
{

const AEVector4 AEVector4::ZERO(0.0F, 0.0F, 0.0F, 0.0F);
const AEVector4 AEVector4::UNIT_X(1.0F, 0.0F, 0.0F, 1.0F);
const AEVector4 AEVector4::UNIT_Y(0.0F, 1.0F, 0.0F, 1.0F);
const AEVector4 AEVector4::UNIT_Z(0.0F, 0.0F, 1.0F, 1.0F);

AEVector4::AEVector4(const AEVector3& inVector)
: mX(inVector.x())
, mY(inVector.y())
, mZ(inVector.z())
, mW(1.0F)
{}

AEVector4& AEVector4::operator=(const AEVector3& inVector)
{
	mX = inVector.x();
	mY = inVector.y();
	mZ = inVector.z();
	mW = 1.0F;

	return *this;
}

AEVector4::operator AEVector3()
{
	if ((mW == 0.0F) || (mW == 1.0F))
		return AEVector3(mX, mY, mZ);
	else
		return AEVector3(mX/mW, mY/mW, mZ/mW);
}

} // namespace AE

