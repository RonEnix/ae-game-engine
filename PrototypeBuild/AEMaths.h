#ifndef AE_MATHS_H_
#define AE_MATHS_H_

#include <math.h>

#include "AESystemTypes.h"

#ifndef M_PI 	// Some math headers don't have this defined.
#define M_PI 3.14159265358979323846
#endif

namespace AE
{

namespace AEMath
{
#if defined(WIN32)
	const AEFloat32 PI						(static_cast<AEFloat32>(M_PI));
#else
	const AEFloat32 PI						(M_PI);
#endif

const AEFloat32 PI_X2					(PI * 2.0F);
const AEFloat32 PI_HALF					(PI * 0.5F);
const AEFloat32 EPSILON					(0.01F);
const AEFloat32 DEGREES_TO_RADIANS		(PI / 180.0F);		// Multiply by degree value to get radian value.
const AEFloat32 RADIANS_TO_DEGREES		(180.0F / PI);		// Multiply by radian value to get degree value.
	
} // namespace AEMath

} // namespace AE


#endif // AE_MATHS_H_

