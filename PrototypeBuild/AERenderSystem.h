#if defined(WIN32)
	#include "AERenderSystemWin32.h"
#elif defined(LINUX)
	#include "AERenderSystemLinux.h"
#elif defined(IOS)
	#error("PLATFORM: No iOS implementation for AERenderSystem")
#elif defined(MAC)
	#error("PLATFORM: No Mac implementation for AERenderSystem")
#else
	#error("PLATFORM: No spefic platform defined for AERenderSystem")
#endif
