#if defined(WIN32)
	#error("PLATFORM: No Windows implementation for AEBitMapFont")
#elif defined(LINUX)
	#include "AEBitMapFontLinux.h"
#elif defined(IOS)
	#error("PLATFORM: No iOS implementation for AEBitMapFont")
#elif defined(MAC)
	#error("PLATFORM: No Mac implementation for AEBitMapFont")
#else
	#error("PLATFORM: No spefic platform defined for AEBitMapFont")
#endif
