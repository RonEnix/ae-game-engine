#ifndef AE_COLOR_H_
#define AE_COLOR_H_

#include "AESystemTypes.h"

namespace AE
{

//TODO: Templatize for other primitive types.
class AEColor
{
	public:
		AEColor() : mRed(0.0F) , mGreen(0.0F) , mBlue(0.0F) , mAlpha(1.0F) {}
		AEColor(const AEFloat32 red, const AEFloat32 green, const AEFloat32 blue, const AEFloat32 alpha)
		: mRed(red), mGreen(green), mBlue(blue), mAlpha(alpha) {}
		AEColor(const AEColor& color) : mRed(color.red()), mGreen(color.green()), mBlue(color.blue()), mAlpha(color.alpha()) {}

		AEFloat32 red()		const { return mRed; }
		AEFloat32 green()	const { return mGreen; }
		AEFloat32 blue()	const { return mBlue; }
		AEFloat32 alpha()	const { return mAlpha; }

		void setRed(const AEFloat32 value)	{ mRed = value; }
		void setGreen(const AEFloat32 value) { mGreen = value; }
		void setBlue(const AEFloat32 value)	{ mBlue = value; }
		void setAlpha(const AEFloat32 value) { mAlpha = value; }

		void set(const AEFloat32 red, const AEFloat32 green, const AEFloat32 blue, const AEFloat32 alpha)
		{ mRed = red; mGreen = green; mBlue = blue; mAlpha = alpha; }

		void setAll(const AEFloat32 value) { mRed = mGreen = mBlue = mAlpha = value; }

		//! Linear interpolation to another color
		AEColor lerp(const AEColor& targetColor, AEFloat32 factor) const { return (*this) * (1.0F - factor) + targetColor * factor; }

		// Binary operators
		AEColor operator+(const AEColor& rColor) const { return AEColor(mRed + rColor.red(), mGreen + rColor.green(), mBlue + rColor.blue(), mAlpha + rColor.alpha()); }
		AEColor operator-(const AEColor& rColor) const { return AEColor(mRed - rColor.red(), mGreen - rColor.green(), mBlue - rColor.blue(), mAlpha - rColor.alpha()); }
		AEColor operator*(const AEColor& rColor) const { return AEColor(mRed * rColor.red(), mGreen * rColor.green(), mBlue * rColor.blue(), mAlpha * rColor.alpha()); }
		AEColor operator/(const AEColor& rColor) const { return AEColor(mRed / rColor.red(), mGreen / rColor.green(), mBlue / rColor.blue(), mAlpha / rColor.alpha()); }

		AEColor operator*(const float value) const { return AEColor(mRed * value, mGreen * value, mBlue * value, mAlpha * value); }
		AEColor operator/(const float value) const { return AEColor(mRed / value, mGreen / value, mBlue / value, mAlpha / value); }

		friend AEColor operator*(const AEFloat32 scaleFactor, const AEColor & rColor) { return rColor * scaleFactor; }

		bool operator==(const AEColor& rColor) const { return ((rColor.red() == mRed) && (rColor.green() == mGreen) && (rColor.blue()) && (rColor.alpha() == mAlpha)) ? true : false; }
		bool operator!=(const AEColor& rColor) const { return !((*this) == rColor); }
		AEColor operator+=(const AEColor& rColor) { (*this) = (*this) + rColor; return (*this); }
		AEColor operator-=(const AEColor& rColor) { (*this) = (*this) - rColor; return (*this); }
		AEColor operator*=(const AEColor& rColor) { (*this) = (*this) * rColor; return (*this); }
		AEColor operator/=(const AEColor& rColor) { (*this) = (*this) / rColor; return (*this); }

		//! return reference to color class from mRed when asked to be set into a pointer variable. eg  AEFloat32* colorPointer(COLOR_BLACK);
		operator AEFloat32* () { return &mRed; }
		//! Const version.
		operator const AEFloat32* () { return &mRed; }

	private:
		AEFloat32 mRed;
		AEFloat32 mGreen;
		AEFloat32 mBlue;
		AEFloat32 mAlpha;
};

const AEColor COLOR_BLACK		(0.0F, 0.0F, 0.0F, 1.0F);
const AEColor COLOR_WHITE		(1.0F, 1.0F, 1.0F, 1.0F);
const AEColor COLOR_RED			(1.0F, 0.0F, 0.0F, 1.0F);
const AEColor COLOR_GREEN		(0.0F, 1.0F, 0.0F, 1.0F);
const AEColor COLOR_BLUE		(0.0F, 0.0F, 1.0F, 1.0F);
const AEColor COLOR_CYAN		(0.0F, 1.0F, 1.0F, 1.0F);
const AEColor COLOR_MAGENTA		(1.0F, 0.0F, 1.0F, 1.0F);
const AEColor COLOR_YELLOW		(1.0F, 1.0F, 0.0F, 1.0F);

} // namespace AE

#endif // AE_COLOR_H_

