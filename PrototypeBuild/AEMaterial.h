#ifndef AE_MATERIAL_H_
#define AE_MATERIAL_H_

#include "AESystemTypes.h"

namespace AE
{

class AEMaterial
{
	public:
		AEMaterial()
		: mMaterialName("INVALID")
		, mAmbientColor(COLOR_BLACK)
		, mDiffuseColor(COLOR_BLACK)
		, mSpecularColor(COLOR_BLACK)
		, mEmissiveColor(COLOR_BLACK)
		, mShininess(0.0F)
		{}

		AEMaterial(const AEString& name, const AEColor& ambientColor, const AEColor& diffuseColor, const AEFloat32 specularPower=0.2F, const AEFloat32 shininess = 0.0F, const AEFloat32 emissivePower=0.0F, const AEFloat32 transparency=1.0F)
		: mMaterialName(name)
		, mAmbientColor(ambientColor)
		, mDiffuseColor(diffuseColor)
		, mSpecularColor(COLOR_BLACK)
		, mEmissiveColor(COLOR_BLACK)
		, mShininess(shininess)
		{
			mDiffuseColor.setAlpha(transparency);
			mSpecularColor.setAll(specularPower);
			mSpecularColor.setAlpha(1.0F);
			mEmissiveColor = mDiffuseColor * emissivePower;
			mEmissiveColor.setAlpha(1.0F);
		}

		const AEString&	getMaterialName()	const { return mMaterialName; }
		const AEColor& getAmbientColor()	const { return mAmbientColor; }
		const AEColor& getDiffuseColor()	const { return mDiffuseColor; }
		const AEColor& getSpecularColor()	const { return mSpecularColor; }
		const AEColor& getEmissiveColor()	const { return mEmissiveColor; }

		AEColor& getAmbientColor()		{ return mAmbientColor; }
		AEColor& getDiffuseColor()		{ return mDiffuseColor; }
		AEColor& getSpecularColor()		{ return mSpecularColor; }
		AEColor& getEmissiveColor()		{ return mEmissiveColor; }

		AEFloat32 getShininess() const { return mShininess; }

	private:
		AEString	mMaterialName;
		AEColor		mAmbientColor;
		AEColor		mDiffuseColor;
		AEColor		mSpecularColor;
		AEColor		mEmissiveColor;

		AEFloat32	mShininess;			//!< 0.0F -> 128.0
};

} // namespace AE

#endif // AE_MATERIAL_H_

