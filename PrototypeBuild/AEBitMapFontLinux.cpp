#include "AEBitMapFontLinux.h"

#include <GL/glx.h>

#include "AELogger.h"

namespace AE
{

const AEString LOG_PREFIX("[AE BITMAP FONT] : ");

AEBitMapFont::~AEBitMapFont()
{
	glDeleteLists(mDisplayIndex, 96);
}

void AEBitMapFont::initialise()
{
	Display* display(XOpenDisplay(NULL));
	XFontStruct* fontInfo(XLoadQueryFont(display, "10x20"));

	if (fontInfo != NULL)
	{
		mDisplayIndex = glGenLists(96);	// Generate list for 96 charactersA
		glXUseXFont(fontInfo->fid, 32, 96, mDisplayIndex);	// Start at space character (at 32) and grab 96 characters from that point on wards.
		XFreeFont(display, fontInfo);
		XCloseDisplay(display);

		mInitialised = true;
	}
	else
	{
		AELOG(LOG_ERROR) << LOG_PREFIX << "Unable to load default font.";
	}

}

void AEBitMapFont::fontPrint(const AEString& text)
{
	if (text == "")
		return;

	glColor3f(0.3F, 0.1F, 0.4F);
	glRasterPos2f(0.0F, 0.0F);
	glPushAttrib(GL_LIST_BIT);
	glListBase(mDisplayIndex - 32);
	glCallLists(text.size(), GL_UNSIGNED_BYTE, text.c_str());
	glPopAttrib();
}

} // namespace AE

