#ifndef AE_SYSTEMS_H_
#define AE_SYSTEMS_H_

#include "AESystemTypes.h"
#include "AESingleton.h"

namespace AE 
{

class AESystems : public AESingleton<AESystems>
{
	friend AESingleton<AESystems>;
	public:
		~AESystems() {}
	private:
		AESystems() {}

};

} // namespace AE

#endif // AE_SYSTEMS_H_
