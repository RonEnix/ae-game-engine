#ifndef AE_FRUSTUM_H_
#define AE_FRUSTUM_H_

#include "AEArray.h"
#include "AEPlane.h"

namespace AE
{

class AEFrustum
{
	public:
		enum FrustumPlanes
		{
			  FRUSTUM_LEFT_PLANE = 0
			, FRUSTUM_RIGHT_PLANE
			, FRUSTUM_TOP_PLANE
			, FRUSTUM_BOTTOM_PLANE
			, FRUSTUM_NEAR_PLANE
			, FRUSTUM_FAR_PLANE
		};

		AEFrustum();
		~AEFrustum();

	private:
		AEFixedArray<AEPlane> mPlanes;
};

} // namespace AE

#endif // AE_FRUSTUM_H_

