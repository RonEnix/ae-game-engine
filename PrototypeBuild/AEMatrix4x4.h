#ifndef AE_MATRIX_4X4_H_
#define AE_MATRIX_4X4_H_

#include "AESystemTypes.h"
#include "AEArray.h"

namespace AE
{

class AEMatrix4x4
{
	public:
		AEMatrix4x4() : mFloatArray(AEFixedArray<AEFloat32>(16U)) { setToZero(); }
		AEMatrix4x4(const AEMatrix4x4& rMatrix) : mFloatArray(rMatrix.mFloatArray) {}
		AEMatrix4x4(const AEFixedArray<AEFloat32>& contents) : mFloatArray(contents) {}
		AEMatrix4x4(const AEFloat32* contents);
		~AEMatrix4x4() {}

		void setValue(const AEUint position, const AEFloat32 value) { mFloatArray.at(position) = value; }
		void setColumn(const AEUint column, const AEVector4& content);
		void setRow(const AEUint row, const AEVector4& content);
		void setToIdentity();
		void setToZero();

		AEFloat32 getValue(const AEUint position) const { return mFloatArray.at(position); }
		AEVector4 getColumn(const AEUint column) const;
		AEVector4 getRow(const AEUint row) const;
		
		// Operators
		friend AEMatrix4x4 operator*(const AEFloat32 scale, const AEMatrix4x4& rMatrix);
		AEMatrix4x4 operator+(const AEMatrix4x4& rMatrix) const;
		AEMatrix4x4 operator-(const AEMatrix4x4& rMatrix) const;
		AEMatrix4x4 operator*(const AEMatrix4x4& rMatrix) const;
		AEMatrix4x4 operator*(const AEFloat32 rValue) const;
		AEMatrix4x4 operator/(const AEFloat32 rValue) const;
		AEMatrix4x4 operator-() const;
		AEMatrix4x4 operator+() const { return (*this); }
		AEVector4 operator*(const AEVector4& rVector) const;
		AEVector3 operator*(const AEVector3 rVector) const;
		//! Refer to a pointer
		operator AEFloat32* () { return mFloatArray; }
		//! Const version.
		operator const AEFloat32* () { return mFloatArray; }
		// Meaningful functions not necessarily useful
		AEVector3 rotateVector3(const AEVector3& vector) const { return (*this) * vector; }
		AEVector4 rotateVector4(const AEVector4& vector) const { return (*this) * vector; }
		AEVector3 translateVector3(const AEVector3& vector) const;

		AEMatrix4x4 transpose() const;
		AEMatrix4x4 inverseTranspose() const;

		bool operator==(const AEMatrix4x4& rMatrix);
		bool operator!=(const AEMatrix4x4& rMatrix);
		void operator+=(const AEMatrix4x4& rMatrix);
		void operator-=(const AEMatrix4x4& rMatrix);
		void operator*=(const AEMatrix4x4& rMatrix);
		void operator*=(const AEFloat32 rValue);
		void operator/=(const AEFloat32 rValue);

	private:
		AEFixedArray<AEFloat32> mFloatArray;
};

}

#endif // AE_MATRIX_4X4_H_

