#ifndef AE_NODE_H_
#define AE_NODE_H_

#include "AEArray.h"

namespace AE
{

//! Scene graph node. Restricted to one parent per node for just simplification for the time being.
class AENode
{
	public:
		const AENode* getParentNode();

	protected:
		void setParentNode(const AENode* parent) { mParent = parent; }
		void addChildNode(const AENode* child);
	private:
		AENode();
		AENode(const AENode&);
		AENode& operator=(const AENode&);
//#if defined(DEBUG)	// Allow string name in debug mode.
//		AEString mNodeName;
//#endif
		AENode* mParent;
		AEArray<AENode*> mChildren;
};

}	// namespace AE

#endif // AE_NODE_H_

