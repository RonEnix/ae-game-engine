#include "AERenderSystemLinux.h"

#include <X11/keysym.h>
#include <X11/Xatom.h>					// Protocol messages.
#include <GL/glu.h>

namespace AE {

AERenderSystem* AERenderSystem::mRenderSystem = NULL;

const AEString LOG_PREFIX("[AE RENDER SYSTEM] : ");

AERenderSystem::AERenderSystem()
: mContextList()
, mDisplay(NULL)
, mVisualInfo(NULL)
, mVideoMode()
, mWindow()
, mScreenHandle(0)
, mScreenWidth(1)
, mScreenHeight(1)
, mFullScreen(false)
, mGlewInitialised(false)
{
}

AERenderSystem::AERenderSystem(const AERenderSystem&)
: mContextList()
, mDisplay(NULL)
, mVisualInfo(NULL)
, mVideoMode()
, mWindow()
, mScreenHandle(0)
, mScreenWidth(1)
, mScreenHeight(1)
, mFullScreen(false)
, mGlewInitialised(false)
{
}

AERenderSystem& AERenderSystem::getInstance()
{
	if (mRenderSystem == NULL)
		mRenderSystem = new AERenderSystem();

	return *mRenderSystem;
}

void AERenderSystem::createWindow(const AEString& title, const AEUint screeWidth, const AEUint screenHeight, const bool fullscreen)
{
	AELOG(LOG_INFO) << LOG_PREFIX << "Creating Window.";

	mFullScreen = fullscreen;
	mScreenWidth = screeWidth;
	mScreenHeight = screenHeight;

	mDisplay = XOpenDisplay(0);
	mScreenHandle = DefaultScreen(mDisplay);

	AEInt minorVideoMode(INVALID);
	AEInt majorVideoMode(INVALID);
	// Get video mode version:
	XF86VidModeQueryVersion(mDisplay, &majorVideoMode, &minorVideoMode);
	// Log out XF86 version:
	AELOG(LOG_INFO) << LOG_PREFIX << "XF86 Video Mode extension version " <<  majorVideoMode << "." << minorVideoMode;
	// Retrieve modes:
	AEInt numberOfModes(0);
	XF86VidModeModeInfo** videoModes;
	XF86VidModeGetAllModeLines(mDisplay, mScreenHandle, &numberOfModes, &videoModes);
	AEInt bestVideoModeIndex(0);
	mVideoMode = *videoModes[bestVideoModeIndex];	// Set to default first mode. We'll find the best shortly. Saves desktop resolutiom.
	// Search for appropriate mode with resolution:
	for (AEInt modeIndex(0); modeIndex < numberOfModes; ++modeIndex)
	{
		if ((videoModes[modeIndex]->hdisplay == static_cast<AEUint16>(screeWidth)) && (videoModes[modeIndex]->vdisplay == static_cast<AEUint16>(screenHeight)))
			bestVideoModeIndex = modeIndex;
	}

	// Settings for GLX visual
	AEInt attributeList[] = 
	{
		GLX_RGBA,
		GLX_DOUBLEBUFFER,	// Ask for double bufferring. We will always assume that this is the norm.
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_DEPTH_SIZE, 16,
		GLX_SAMPLES, 3,
		None
	};
	// Ask for new visual info with the above attributes:
	mVisualInfo = glXChooseVisual(mDisplay, mScreenHandle, attributeList);
	// Check if we can use double bufferring if the attribute list we sent was a success.
	if (mVisualInfo != NULL)
		AELOG(LOG_INFO) << LOG_PREFIX << "Double bufferring visual information available.";
	else
		AELOG(LOG_INFO) << LOG_PREFIX << "Single bufferring visual information available.";

	// Log the GLX version:
	AEInt glxVersionMajor(INVALID);
	AEInt glxVersionMinor(INVALID);
	glXQueryVersion(mDisplay, &glxVersionMajor, &glxVersionMinor);
	AELOG(LOG_INFO) << LOG_PREFIX << "GLX Version " << glxVersionMajor << "." << glxVersionMinor;
	// Apply window attributes:
	XSetWindowAttributes windowAttributes;
	windowAttributes.colormap = XCreateColormap(mDisplay, RootWindow(mDisplay, mVisualInfo->screen), mVisualInfo->visual, AllocNone);
	windowAttributes.border_pixel = 0;

	if (mFullScreen)
	{
		// Switch to the best mode:
		XF86VidModeSwitchToMode(mDisplay, mScreenHandle, videoModes[bestVideoModeIndex]);
		XF86VidModeSetViewPort(mDisplay, mScreenHandle, 0, 0);
		AELOG(LOG_INFO) << LOG_PREFIX << "Fullscreen resolution: " << mVideoMode.hdisplay << "x" << mVideoMode.vdisplay;
		// Windows attribute settings:
		windowAttributes.override_redirect = True; // True value is a GLX type.
		windowAttributes.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask;	// Window events to read.
		mWindow = XCreateWindow(mDisplay,
								RootWindow(mDisplay, mVisualInfo->screen),
								0, 0,															// Screen (x,y) position (top-left origin)
								videoModes[bestVideoModeIndex]->hdisplay,						// Screen width.
								videoModes[bestVideoModeIndex]->vdisplay,						// Screen height.
								0,																// Border width.
								mVisualInfo->depth,												// Screen depth.
								InputOutput,													// Windows class input/output.
								mVisualInfo->visual,
								CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect,	// Windows mask.
								&windowAttributes);
		XWarpPointer(mDisplay,			// X server display.
					 None,				// Window source.
					 mWindow, 			// Destination window.
					 0,					// Source x position.
					 0,					// Source y position.
					 0,					// Width.
					 0,					// Height.
					 0,					// Destination x.
					 0);				// Destination y.
		XMapRaised(mDisplay, mWindow);
		XGrabKeyboard(mDisplay, mWindow, True, GrabModeAsync, GrabModeAsync, CurrentTime);
		XGrabPointer(mDisplay, mWindow, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, mWindow, None, CurrentTime); 
	}
	else
	{
		windowAttributes.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask;
		mWindow = XCreateWindow(mDisplay,
								RootWindow(mDisplay, mVisualInfo->screen),
								0, 0,
								mScreenWidth,
								mScreenHeight,
								0,
								mVisualInfo->depth,
								InputOutput,
								mVisualInfo->visual,
								CWBorderPixel | CWColormap | CWEventMask,
								&windowAttributes);

		Atom wmDelete(XInternAtom(mDisplay, "WM_DELETE_WINDOW", True));
		XSetWMProtocols(mDisplay, mWindow, &wmDelete, 1);
		XSetStandardProperties(mDisplay, mWindow, title.c_str(), title.c_str(), None, NULL, 0, NULL);
		XMapRaised(mDisplay, mWindow);
	}

	XFree(videoModes);
}

void AERenderSystem::destroyWindow()
{
	if (mContextList.size() > 0)
	{
		if (!glXMakeCurrent(mDisplay, None, NULL))
		{
			AELOG(LOG_ERROR) << LOG_PREFIX << "Could not release context.";
		}

		for (std::vector<GLXContext>::iterator itrContext(mContextList.begin()); itrContext != mContextList.end(); ++itrContext)
		{
			AELOG(LOG_INFO) << LOG_PREFIX << "Context at mem address " << std::hex << *itrContext << " destroyed.";
			glXDestroyContext(mDisplay, *itrContext);
			*itrContext = NULL;
		}
	}

	mContextList.clear();

	if (mFullScreen)
	{
		XF86VidModeSwitchToMode(mDisplay, mScreenHandle, &mVideoMode);              
		XF86VidModeSetViewPort(mDisplay, mScreenHandle, 0, 0);
	}
	XCloseDisplay(mDisplay);
	AELOG(LOG_INFO) << LOG_PREFIX << "Display and window clean destroy.";
}

AEContextHandle AERenderSystem::createRenderContext()
{
	// All render context that needs to be created needs to be done before any call to setCurrentContext()
	// as the function will release the visual info data to conserve memory.
	// Check if visual information exists. If not exit with log warning, and don't assert.
	if (mVisualInfo != NULL)
	{
		// Context handle is simply the index value in the storage.
		mContextList.push_back(glXCreateContext(mDisplay, mVisualInfo, 0, GL_TRUE));
		return static_cast<AEContextHandle>(mContextList.size());
	}

	return INVALID;
}

void AERenderSystem::setCurrentContext(const AEContextHandle contextHandle)
{
	// Function will free visial info
	if (mVisualInfo != NULL)
	{
		XFree(mVisualInfo);
		mVisualInfo = NULL;
	}

	const GLXContext& currentContext(mContextList.at(contextHandle - 1));

	glXMakeCurrent(mDisplay, mWindow, currentContext);

	if (glXIsDirect(mDisplay, currentContext))
	{
		AELOG(LOG_INFO) << LOG_PREFIX << "Context handle " << contextHandle << " set. DRI enabled.";
	}
	else
	{
		AELOG(LOG_INFO) << LOG_PREFIX << "Context handle " << contextHandle << " set. DRI not available.";
	}

	if (mGlewInitialised == false)
	{ // Initialise GLEW
		GLenum error(glewInit());
		if (GLEW_OK != error)
		{
			AELOG(LOG_INFO) << LOG_PREFIX << "Failed to initialise GLEW. " << glewGetErrorString(error) << ".";
		}
		else
		{
			AELOG(LOG_INFO) << LOG_PREFIX << "GLEW Version: " << glewGetString(GLEW_VERSION) << ".";
			mGlewInitialised = true;
		}
	}
}

void AERenderSystem::initialiseRenderer()
{
	// Default OpenGL enabled modes
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glFrontFace(GL_CCW);							// Force counter-clockwise winding fronts.
	glClearDepth(1.0F);

	//glEnable(GL_TEXTURE_2D);

	//glEnable(GL_COLOR_MATERIAL);
	//glColorMaterial(GL_FRONT_AND_BACK, GL_EMISSION);
	
	{	// Temp stuff
		// Multisampling
		//glEnable(GL_MULTISAMPLE_ARB);
	}

	// Perspective calculation setting:
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resizeRenderSystem(mScreenWidth, mScreenHeight);
	glFlush();
}

void AERenderSystem::setClearColor(const AEColor& color)
{
	glClearColor(color.red(), color.blue(), color.green(), color.alpha());
}

void AERenderSystem::enableRenderParameter(const AEUint params)
{
	if (RENDER_PARAM_DEPTH_TEST & params)
		enableDepthTest();
	if (RENDER_PARAM_BACK_FACE_CULLING & params)
		enableBackFaceCulling();
	if (RENDER_PARAM_ALPHA_BLEND & params)
		enableAlphaBlend();
	if (RENDER_PARAM_LINE_SMOOTHING & params)
		enableLineSmoothing();
	if (RENDER_PARAM_WIRE_FRAME & params)
		enableWireFrameMode();
	if (RENDER_PARAM_LIGHTING & params)
		enableLighting();
	if (RENDER_PARAM_TEXTURING & params)
		enableTexture();
}

void AERenderSystem::disableRenderParameter(const AEUint params)
{
	if (RENDER_PARAM_DEPTH_TEST & params)
		disableDepthTest();
	if (RENDER_PARAM_BACK_FACE_CULLING & params)
		disableBackFaceCulling();
	if (RENDER_PARAM_ALPHA_BLEND & params)
		disableAlphaBlend();
	if (RENDER_PARAM_LINE_SMOOTHING & params)
		disableLineSmoothing();
	if (RENDER_PARAM_WIRE_FRAME & params)
		disableWireFrameMode();
	if (RENDER_PARAM_LIGHTING & params)
		disableLighting();
	if (RENDER_PARAM_TEXTURING & params)
		disableTexture();
}

void AERenderSystem::enableLight(const AEInt light)
{
	switch (light)
	{
		case 0:
			glEnable(GL_LIGHT0);
			break;
		case 1:
			glEnable(GL_LIGHT1);
			break;
		case 2:
			glEnable(GL_LIGHT2);
			break;
		case 3:
			glEnable(GL_LIGHT3);
			break;
		case 4:
			glEnable(GL_LIGHT4);
			break;
		case 5:
			glEnable(GL_LIGHT5);
			break;
		case 6:
			glEnable(GL_LIGHT6);
			break;
		case 7:
			glEnable(GL_LIGHT7);
			break;
		default:
			break;
	}
}

void AERenderSystem::disableLight(const AEInt light)
{
	switch (light)
	{
		case 0:
			glDisable(GL_LIGHT0);
			break;
		case 1:
			glDisable(GL_LIGHT1);
			break;
		case 2:
			glDisable(GL_LIGHT2);
			break;
		case 3:
			glDisable(GL_LIGHT3);
			break;
		case 4:
			glDisable(GL_LIGHT4);
			break;
		case 5:
			glDisable(GL_LIGHT5);
			break;
		case 6:
			glDisable(GL_LIGHT6);
			break;
		case 7:
			glDisable(GL_LIGHT7);
			break;
		default:
			break;
	}
}

void AERenderSystem::startFrame()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
}

void AERenderSystem::endFrame()
{
	glFinish();
	glXSwapBuffers(mDisplay, mWindow);
}

void AERenderSystem::resizeRenderSystem(AEInt width, AEInt height)
{
	if (height == 0)
		height = 1;

	glViewport(0.0F, 0.0F, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0F, static_cast<GLfloat>(width)/static_cast<GLfloat>(height), 0.01F, 100.0F);
	glMatrixMode(GL_MODELVIEW);

}

void AERenderSystem::enableDepthTest()
{
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
}

void AERenderSystem::enableBackFaceCulling()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void AERenderSystem::enableAlphaBlend()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void AERenderSystem::enableLineSmoothing()
{
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glLineWidth(1.0F);
}

void AERenderSystem::enableWireFrameMode()
{
	glPolygonMode(GL_FRONT, GL_LINE);
}

void AERenderSystem::enableLighting()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, AEColor(0.1F, 0.1F, 0.4F, 1.0F));
}

void AERenderSystem::enableTexture()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void AERenderSystem::disableDepthTest()
{
	glDisable(GL_DEPTH_TEST);
	glClearDepth(1.0F);
}

void AERenderSystem::disableBackFaceCulling()
{
	glDisable(GL_CULL_FACE);
}

void AERenderSystem::disableAlphaBlend()
{
	glDisable(GL_BLEND);
}

void AERenderSystem::disableLineSmoothing()
{
	glDisable(GL_LINE_SMOOTH);
	glLineWidth(1.0F);
}

void AERenderSystem::disableWireFrameMode()
{
	glPolygonMode(GL_FRONT, GL_FILL);
}

void AERenderSystem::disableLighting()
{
	glDisable(GL_LIGHTING);
}

void AERenderSystem::disableTexture()
{
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

//! Handles only event inputs occurring for the window.
bool AERenderSystem::handleWindowEvent()
{
	XEvent event;
	bool exitWindowHandle(false);

	while (XPending(mDisplay) > 0)
	{
		XNextEvent(mDisplay, &event);
		switch (event.type)
		{
			case ClientMessage:
				if (*XGetAtomName(mDisplay, event.xclient.message_type) == *"WM_PROTOCOLS")
					exitWindowHandle = true;
			break;
			case KeyPress:
				if (XLookupKeysym(&event.xkey, 0) == XK_Escape)
					exitWindowHandle = true;
			break;
			case ConfigureNotify:
				if ((event.xconfigure.width != mScreenWidth) || (event.xconfigure.height != mScreenHeight))
				{
					mScreenWidth = event.xconfigure.width;
					mScreenHeight = event.xconfigure.height;
					AELOG(LOG_INFO) << LOG_PREFIX << "Resizing Window: " << mScreenWidth << "x" << mScreenHeight << ".";
					resizeRenderSystem(mScreenWidth, mScreenHeight);
				}
			break;
		}
	}
	return exitWindowHandle;
}

} // namespace AE

