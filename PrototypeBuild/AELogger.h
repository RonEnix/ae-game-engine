#ifndef AE_LOGGER_H_
#define AE_LOGGER_H_

#include <sstream>
#include <fcntl.h>
#ifdef WIN32
	#include <io.h>
#endif
#include "AESystemTypes.h"
#include "AESingleton.h"

#ifdef WIN32
	#include <windows.h>
#endif

namespace AE
{

enum AELoggerLevel
{
	  LOG_INFO = 0
	, LOG_WARNING
	, LOG_DEBUG
	, LOG_DEBUG_DEEP
	, LOG_ERROR
	, LOG_ALL
};


#define LOG_DETAILS "[Src: " << __FILE__ << "][Ln: " << __LINE__ << "] "

static AELoggerLevel LOG_LEVEL(LOG_ALL);

#if defined(LINUX)
static AEString LOG_COLOR_DEFAULT("\033[0m");
static AEString LOG_COLOR_BLACK("\033[0;30m");
static AEString LOG_COLOR_RED("\033[0;31m");
static AEString LOG_COLOR_GREEN("\033[0;32m");
static AEString LOG_COLOR_BROWN("\033[0;33m");
static AEString LOG_COLOR_BLUE("\033[0;34m");
static AEString LOG_COLOR_MAGENTA("\033[0;35m");
static AEString LOG_COLOR_CYAN("\033[0;36m");
static AEString LOG_COLOR_GRAY("\033[0;37m");
static AEString LOG_TEXT_BOLD("\033[1m");
static AEString LOG_TEXT_UNDERLINE("\033[4m");
static AEString LOG_TEXT_BACKGROUND("\033[7m");
static AEString LOG_TEXT_STRIKE("\033[9m");
static AEString ENDL("\n");
#elif defined(WIN32)
static AEString LOG_COLOR_DEFAULT;
static AEString LOG_COLOR_BLACK;
static AEString LOG_COLOR_RED;
static AEString LOG_COLOR_GREEN;
static AEString LOG_COLOR_BROWN;
static AEString LOG_COLOR_BLUE;
static AEString LOG_COLOR_MAGENTA;
static AEString LOG_COLOR_CYAN;
static AEString LOG_COLOR_GRAY;
static AEString LOG_TEXT_BOLD;
static AEString LOG_TEXT_UNDERLINE;
static AEString LOG_TEXT_BACKGROUND;
static AEString LOG_TEXT_STRIKE;
static AEString ENDL("\n");
#endif

#if defined (LINUX)
	#define AELOG_INIT
	#define AELOG_DESTROY

#elif defined(WIN32)
	#define AELOG_INIT\
		AllocConsole();\
		HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);\
		CONSOLE_SCREEN_BUFFER_INFO consoleInfo;\
		GetConsoleScreenBufferInfo(handle, &consoleInfo);\
		consoleInfo.dwSize.X = static_cast<SHORT>(1024U);\
		consoleInfo.dwSize.Y = static_cast<SHORT>(1024U);\
		SetConsoleScreenBufferSize(handle, consoleInfo.dwSize);\
		SetConsoleMode(handle, ENABLE_EXTENDED_FLAGS | ENABLE_QUICK_EDIT_MODE);\
		const int consoleHandle(_open_osfhandle(reinterpret_cast<intptr_t>(handle), _O_TEXT));\
		FILE* fp(_fdopen(consoleHandle, "w"));\
		*stdout = *fp;\
		setvbuf(stdout, NULL, _IONBF, 0 );\
		SetConsoleTitle("AE Logs")

	#define AELOG_DESTROY\
		FreeConsole()
			
#endif

class AELogger
{
	public:
		AELogger()
		: mOutputStringStream()
		, mLogLevel(LOG_INFO)
		#ifdef WIN32
		, mConsoleHandle(0)
		#endif
		{
			#ifdef WIN32
				mConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
			#endif
		}

		virtual ~AELogger()
		{
			mOutputStringStream << ENDL;
			mOutputStringStream << LOG_COLOR_DEFAULT;
			#if defined(LINUX)
				fprintf(stderr, "%s", mOutputStringStream.str().c_str());
				fflush(stderr);
			#elif defined(WIN32)
				DWORD charactersWritten(0);
				WriteConsole(mConsoleHandle, mOutputStringStream.str().c_str(), mOutputStringStream.str().length(), &charactersWritten, NULL);
			#endif
		}

		std::ostringstream& getOutputStream(const AELoggerLevel logLevel)
		{
			switch (logLevel)
			{
				case LOG_ERROR:
					mOutputStringStream << LOG_COLOR_RED << "[AELOG:ERROR] " << LOG_COLOR_DEFAULT << LOG_DETAILS;
					break;
				case LOG_WARNING:
					mOutputStringStream << LOG_COLOR_MAGENTA << "[AELOG:WARNING] " << LOG_COLOR_DEFAULT << LOG_DETAILS;
					break;
				case LOG_INFO:
					mOutputStringStream << LOG_COLOR_GREEN << "[AELOG:INFO] " << LOG_COLOR_DEFAULT;
					break;
				case LOG_DEBUG:
					mOutputStringStream << LOG_COLOR_CYAN << "[AELOG:DEBUG] " << LOG_COLOR_DEFAULT;
					break;
				case LOG_DEBUG_DEEP:
					mOutputStringStream << LOG_COLOR_BLUE << "[AELOG:DEBUG DEEP] " << LOG_COLOR_DEFAULT;
					break;
				default:
					break;
			}
			mLogLevel = logLevel;

			return mOutputStringStream;
		}

	private:
		AELogger(const AELogger&);
		AELogger& operator=(const AELogger&);

		std::ostringstream mOutputStringStream;
		AELoggerLevel mLogLevel;
		#ifdef WIN32
			HANDLE mConsoleHandle;
		#endif
};

// Macro helper
#define AELOG(level) \
if (level > LOG_LEVEL) ; \
else AELogger().getOutputStream(level)

}

#endif // AE_LOGGER_H_

