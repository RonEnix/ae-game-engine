#ifndef AE_SYSTEM_TYPES_H_
#define AE_SYSTEM_TYPES_H_

#include <stdio.h>
#include <iostream>

namespace AE
{
typedef	  signed char			AEInt8;
typedef unsigned char			AEUint8;
typedef 	 AEUint8			AEByte;
typedef   signed short			AEInt16;
typedef unsigned short			AEUint16;
typedef unsigned int			AEUint;
typedef   signed int			AEInt;
typedef   signed long			AEInt32;
typedef unsigned long			AEUint32;
typedef float					AEFloat32;
typedef double					AEFloat64;
typedef std::basic_string<char>	AEString;
typedef AEUint					AEContextHandle;

const AEUint INVALID(static_cast<AEUint>(-1)); // Make a high value invalid number.

}	// namespace AE

#include "AEColor.h"
#include "AEMaths.h"
#include "AEVector3.h"
#include "AEVector4.h"

#endif // AE_SYSTEM_TYPES_H_
