#include "AEVector3.h"

#include "AESystemTypes.h"

namespace AE
{

const AEVector3 AEVector3::ZERO(0.0F, 0.0F, 0.0F);
const AEVector3 AEVector3::UNIT_X(1.0F, 0.0F, 0.0F);
const AEVector3 AEVector3::UNIT_Y(0.0F, 1.0F, 0.0F);
const AEVector3 AEVector3::UNIT_Z(0.0F, 0.0F, 1.0F);

void AEVector3::normalize()
{
	const AEFloat32 length(AEVector3::length());
	if ((length == 1.0F) || (length == 0.0F))
		return;

	const AEFloat32 factorScale(1.0F / length);
	mX *= factorScale;
	mY *= factorScale;
	mZ *= factorScale;
}

AEVector3 AEVector3::getNormalized() const 
{
	// Returns a normalized vector. Does not change internal member values.
	AEVector3 copyVector(*this);
	copyVector.normalize();

	return copyVector;
}

AEVector3 AEVector3::getXRotated(const AEFloat64 radians) const
{
	if (radians == 0.0F)
		return (*this);

	const AEFloat32 sine(static_cast<AEFloat32>(sin(radians)));
	const AEFloat32 cosine(static_cast<AEFloat32>(cos(radians)));

	return AEVector3(mX, (mY * cosine) - (mZ * sine), (mY * sine) + (mZ * cosine)); 
}

AEVector3 AEVector3::getYRotated(const AEFloat64 radians) const
{
	if (radians == 0.0F)
		return (*this);

	const AEFloat32 sine(static_cast<AEFloat32>(sin(radians)));
	const AEFloat32 cosine(static_cast<AEFloat32>(cos(radians)));

	return AEVector3((mX * cosine) + (mZ * sine), mY, (-mX * sine) + (mZ * cosine)); 
}

AEVector3 AEVector3::getZRotated(const AEFloat64 radians) const
{
	if (radians == 0.0F)
		return (*this);

	const AEFloat32 sine(static_cast<AEFloat32>(sin(radians)));
	const AEFloat32 cosine(static_cast<AEFloat32>(cos(radians)));

	return AEVector3((mX * cosine) - (mY * sine), (mX * sine) + (mZ * sine), mZ); 
}

} // namespace AE

