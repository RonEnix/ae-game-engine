#ifndef AE_VECTOR_4_
#define AE_VECTOR_4_

#include "AESystemTypes.h"

namespace AE 
{

class AEVector3;

//! float32 4d vector class.
class AEVector4
{
	public:
		AEVector4() : mX(0.0F) , mY(0.0F), mZ(0.0F), mW(1.0F) {}
		AEVector4(const AEFloat32 xValue, const AEFloat32 yValue, const AEFloat32 zValue, const AEFloat32 wValue) : mX(xValue), mY(yValue), mZ(zValue), mW(wValue) {}
		AEVector4(const AEVector4& vector) : mX(vector.x()), mY(vector.y()), mZ(vector.z()), mW(vector.w()) {}
		//! Allow Vector3 to Vector4.
		AEVector4(const AEVector3& inVector);
		AEVector4& operator=(const AEVector3& inVector);
		//! Allow Vector4 to Vector3.
		operator AEVector3();

		AEFloat32 x() const { return mX; }
		AEFloat32 y() const { return mY; }
		AEFloat32 z() const { return mZ; }
		AEFloat32 w() const { return mW; }

		void setX(const AEFloat32 value) { mX = value; }
		void setY(const AEFloat32 value) { mY = value; }
		void setZ(const AEFloat32 value) { mZ = value; }
		void setW(const AEFloat32 value) { mW = value; }

		void setXYZW(const AEFloat32 x, const AEFloat32 y, const AEFloat32 z, const AEFloat32 w) { mX = x; mY = y; mZ = z; mW = w; }
		void setAll(const AEFloat32 value) { mX = mY = mZ = mW = value; }

		AEFloat32 dotProduct(const AEVector4& rVector) const
		{ return (mX * rVector.x()) + (mY * rVector.y()) + (mZ * rVector.z()) + (mW * rVector.w()); }

		//! Simple yet very useful linear interpolation to another vector.
		AEVector4 lerp(const AEVector4& vector2, const AEFloat32 factor) const { return (*this) * (1.0F - factor) + (vector2 * factor); }

		//! Quadratic interpolation.
		AEVector4 qerp(const AEVector4& vector2, const AEVector4& vector3, const AEFloat32 factor)
		{ return (*this) * (1.0F - factor) * (1.0F - factor) + 2.0F * vector2 * factor * (1.0F - factor) + vector3 * factor * factor; }

		// Binary operators
		AEVector4 operator+(const AEVector4& rVector) const { return AEVector4(mX + rVector.x(), mY + rVector.y(), mZ + rVector.z(), mW + rVector.w()); }
		AEVector4 operator-(const AEVector4& rVector) const { return AEVector4(mX - rVector.x(), mY - rVector.y(), mZ - rVector.z(), mW - rVector.w()); }
		AEVector4 operator*(const AEFloat32& value) const { return AEVector4(mX * value, mY * value, mZ * value, mW * value); }
		AEVector4 operator/(const AEFloat32& value) const { return (value == 0.0F) ? AEVector4::ZERO : AEVector4(mX / value, mY / value, mZ / value, mW / value); }

		friend AEVector4 operator*(const AEFloat32& value, const AEVector4& rVector) { return rVector * value; }

		bool operator==(const AEVector4& rVector) const { return ((mX == rVector.x()) && (mY == rVector.y()) && (mZ == rVector.z()) && (mW == rVector.w())) ? true : false; }
		bool operator!=(const AEVector4& rVector) const { return !((*this) == rVector); }
		void operator+=(const AEVector4& rVector) { mX += rVector.x(); mY += rVector.y(); mZ += rVector.z(); mW += rVector.w(); }
		void operator-=(const AEVector4& rVector) { mX -= rVector.x(); mY -= rVector.y(); mZ -= rVector.z(); mW -= rVector.w(); }
		void operator*=(const AEFloat32 value) { mX *= value; mY *= value; mZ *= value; mW *= value; }
		void operator/=(const AEFloat32 value) { if (value == 0.0F) return; else mX /= value; mY /= value; mZ /= value; mW /= value; }

		// Unary operators (switches values)
		AEVector4 operator-(void) const { return AEVector4(-mX, -mY, -mZ, -mW); }
		AEVector4 operator+(void) const { return *this; }
		
		//! Refer to a pointer
		operator AEFloat32* () { return &mX; }
		//! Const version.
		operator const AEFloat32* () { return &mX; }

		static const AEVector4 ZERO;
		static const AEVector4 UNIT_X;
		static const AEVector4 UNIT_Y;
		static const AEVector4 UNIT_Z;

	private:
		AEFloat32 mX;
		AEFloat32 mY;
		AEFloat32 mZ;
		AEFloat32 mW;
};

} // namespace AE

#endif // AE_VECTOR_4_

