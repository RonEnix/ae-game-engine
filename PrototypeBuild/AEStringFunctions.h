#ifndef AE_STRING_FUNCTIONS_H_
#define AE_STRING_FUNCTIONS_H_

#include "AESystemTypes.h"

namespace AE
{

const AEString intToString(const int value, const int baseValue=10);
const AEString intToString(const long value, const int baseValue=10);
const AEString unsignedIntToString(const unsigned int value, const int baseValue=10);

}

#endif // AE_STRING_FUNCTIONS_H_
