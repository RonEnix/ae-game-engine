#ifndef AE_SINGLETON_H_
#define AE_SINGLETON_H_

namespace AE
{

template<class T>
class Singleton
{
	public:
		static T& instance()
		{
			if (!instance_)
				init();

			return *instance_;
		}

		static void destroy()
		{
			delete instance_;
		}

	protected:
		Singleton() {}
		virtual ~Singleton() {}

	private:
		static T* instance_;
		static void init()
		{
			if (instance_ == NULL) {
				instance_ = new T();
			}
		}
};

template<class T> T* Singleton<T>::instance_(0);

}

#endif // AE_SINGLETON_H_

