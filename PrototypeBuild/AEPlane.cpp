#include "AEPlane.h"

#include "AELogger.h"

namespace AE
{

void AEPlane::generateFromPoints(const AEVector3& point1, const AEVector3& point2, const AEVector3& point3)
{
	AEVector3 deltaP2P1(point2 - point1);
	AEVector3 deltaP3P1(point3 - point1);

	mNormal = deltaP2P1.crossProduct(deltaP3P1);
	mNormal.normalize();

	if (mNormal.length() <= 0.0F)
	{
		AELOG(LOG_ERROR) << "[AE PLANE] Plane normal has no length. Cannot use Plane parameters.";
		return;
	}

	mDistance = -(mNormal.dotProduct(point1));
}

bool AEPlane::operator==(const AEPlane& rPlane) const
{
	if ((mNormal == rPlane.mNormal) && (mDistance == rPlane.mDistance))
		return true;

	return false;
}

bool AEPlane::operator!=(const AEPlane& rPlane) const
{
	return !((*this) == rPlane);
}

AEPlane::IntersectStatus AEPlane::checkPointIntersect(const AEVector3& point) const
{
	const AEFloat32 distance(point.x() * mNormal.x() + point.y() * mNormal.y() + point.z() * mNormal.z() + mDistance);

	if (distance > AEMath::EPSILON)
		return POINT_FRONT_OF_PLANE;

	if (distance < -AEMath::EPSILON)
		return POINT_BEHIND_PLANE;

	return POINT_ON_PLANE;
}

}

