#ifndef AE_RENDER_SYSTEM_BASE_H_
#define AE_RENDER_SYSTEM_BASE_H_

#include "AESystemTypes.h"
#include "AELogger.h"

namespace AE {

//Render Params
const AEUint RENDER_PARAM_DEPTH_TEST		(1);
const AEUint RENDER_PARAM_BACK_FACE_CULLING (1 << 1);
const AEUint RENDER_PARAM_ALPHA_BLEND		(1 << 2);
const AEUint RENDER_PARAM_LINE_SMOOTHING	(1 << 3);
const AEUint RENDER_PARAM_WIRE_FRAME		(1 << 4);
const AEUint RENDER_PARAM_LIGHTING			(1 << 5);
const AEUint RENDER_PARAM_TEXTURING			(1 << 6);

//! Pure virtual class defining the structure of derived render systems.
class AERenderSystemBase
{
	public:
		AERenderSystemBase() {}
		virtual ~AERenderSystemBase() {}

		virtual void createWindow(const AEString& title, const AEUint width, const AEUint height, const bool fullscreen) = 0;
		virtual void destroyWindow() = 0;
		virtual void setCurrentContext(const AEContextHandle contextHandle) = 0;
		virtual AEContextHandle createRenderContext() = 0;
		virtual void initialiseRenderer() = 0;

		virtual void setClearColor(const AEColor& color) = 0;

		virtual void enableRenderParameter(const AEUint params) = 0;
		virtual void disableRenderParameter(const AEUint params) = 0;

		virtual void enableLight(const AEInt light) = 0;
		virtual void disableLight(const AEInt light) = 0;

		virtual void startFrame() = 0;
		virtual void endFrame() = 0;

		virtual void resizeRenderSystem(AEInt width, AEInt height) = 0;
};

}	// namespace AE

#endif // AE_RENDER_SYSTEM_BASE_H_

