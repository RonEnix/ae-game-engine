# AE GAME ENGINE README

## Brief:

- New game engine repository.
- Cross-platform engine(Win32,Linux,Android,iOS,Mac).
- Initial progress only on Linux operating system.

## Requirements:

- Scons Build Environment Tool. Extending to CMake and then finilising build choice.
- Linux specific X11 headers: libxxf86vm-dev
- Current test build uses GLU. Future build will not require this so install the appropriate dependency for build.

## Building:

- Since this project uses SOIL and further more libraries in the future I have placed all the dependencies in an organised folder.
- You need to download the engine library file in the [Downloads](https://bitbucket.org/RonEnix/ae-game-engine/downloads) section.
- Extract to the same level as the game engine folder.
- Rename folder to "library".

## What you should see:

- Numerous 3D objects. Grid display similar to many 3D modelling applications and a placent gizmo(non-interactable)
![AE Initial screenshot](http://i13.photobucket.com/albums/a280/ky21185/Screenshot-2.png)


## ToDo:

- Sub-system shell code: (Not in any particular order, and not complete)
	- Render System
	- File System
		- Resource Manager
		- ...
	- Input System
	- Audio System

